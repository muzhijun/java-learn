package com.example.eventlistener;

import com.example.eventlistener.event.PushEvent;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;

@Slf4j
@SpringBootTest
class EventlistenerApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void publish() {
        String msg = "测试数据";
        log.info("发布信息：" + msg);
        ApplicationEvent event = new PushEvent(this, msg);
        applicationContext.publishEvent(event);
    }

}
