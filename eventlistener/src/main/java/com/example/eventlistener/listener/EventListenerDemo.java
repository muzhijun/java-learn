package com.example.eventlistener.listener;

import com.example.eventlistener.event.PushEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/4 14:18
 */
@Service
@Slf4j
public class EventListenerDemo {

    @EventListener(PushEvent.class)
    public void eventListener(PushEvent event) {
        log.info(this.getClass().getSimpleName() + "监听到数据：" + event.getMsg());
    }
}
