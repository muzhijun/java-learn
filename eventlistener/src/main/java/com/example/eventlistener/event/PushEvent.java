package com.example.eventlistener.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/4 14:15
 */
public class PushEvent extends ApplicationEvent {

    private String msg ;


    public PushEvent(Object source, String msg) {
        super(source);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
