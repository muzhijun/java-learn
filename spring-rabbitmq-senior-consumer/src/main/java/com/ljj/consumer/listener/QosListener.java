package com.ljj.consumer.listener;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

@Component
public class QosListener implements ChannelAwareMessageListener {

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        Thread.sleep(1000);
        long deliveryTag = message.getMessageProperties().getDeliveryTag();

        try {
            System.out.println("接收到消息："+ new String(message.getBody()));

            System.out.println("处理业务逻辑...");
            /*
            basicAck(long deliveryTag, boolean multiple)
            参数：
                1 delivertyTag tag标识
                2 multiple 是否签收多个
             */
            channel.basicAck(deliveryTag, true);
        } catch (Exception e){
            //发生异常，拒绝签收
            /*
            basicNack(long deliveryTag, boolean multiple, boolean requeue)
            参数:
                1 deliveryTag tag标识
                2 multiple 是否签收多个
                3 requeue 是否退回队列
             */
            channel.basicNack(deliveryTag, true, true);
            //拒绝签收后消息退回到队列中，重新被监听器捕获，继续消费
        }

    }
}
