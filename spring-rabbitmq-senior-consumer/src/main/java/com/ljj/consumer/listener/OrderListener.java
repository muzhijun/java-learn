package com.ljj.consumer.listener;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

@Component
public class OrderListener implements ChannelAwareMessageListener {

    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        long deliveryTag = message.getMessageProperties().getDeliveryTag();

        try {
            System.out.println("接收到消息："+ new String(message.getBody()));
            System.out.println("处理业务逻辑...");
            channel.basicAck(deliveryTag, true);
        } catch (Exception e){
            channel.basicNack(deliveryTag, true, false);
        }

    }
}
