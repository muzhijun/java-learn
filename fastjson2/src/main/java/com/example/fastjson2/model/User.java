package com.example.fastjson2.model;


import lombok.Getter;
import lombok.Setter;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/5/6 21:41
 */
@Getter
@Setter
public class User {

    private String name;
    private Integer age;
    private String email;
    private String address;
}
