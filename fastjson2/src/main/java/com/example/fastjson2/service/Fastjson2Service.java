package com.example.fastjson2.service;

import com.alibaba.fastjson2.JSON;
import com.example.fastjson2.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/5/6 21:40
 */
@Service
public class Fastjson2Service {

    public static void main(String[] args) {
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setName("张三");
        user.setAge(20);
        user.setEmail("111@qq.com");
        user.setAddress("合肥市高新区");
        userList.add(user);

        System.out.println(JSON.toJSONString(userList));


    }
}
