package com.netty.example;

import io.netty.channel.DefaultEventLoop;
import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.GenericFutureListener;
import lombok.extern.slf4j.Slf4j;

/*
    异步处理任务
 */
@Slf4j
public class PromiseDemo1 {

    public static void main(String[] args) {

        DefaultEventLoop eventExecutors = new DefaultEventLoop();
        DefaultPromise<Integer>  promise = new DefaultPromise<>(eventExecutors);

        //为promise添加监听事件
        promise.addListener((GenericFutureListener)future -> {
            log.debug("{}", future.getNow());
        });

        eventExecutors.execute(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //手动设置
            log.debug("set success{}", 10);
            promise.setSuccess(10);
        });

        log.debug("start...");
    }
}
