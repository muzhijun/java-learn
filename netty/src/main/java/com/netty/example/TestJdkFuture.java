package com.netty.example;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Slf4j
public class TestJdkFuture {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //创建一个固定线程数的线程池
        ExecutorService service = Executors.newFixedThreadPool(2);
        //提交执行任务
        Future<Integer> future = service.submit(() -> {
            log.debug("执行计算结果");
            return 50;
        });

        log.debug("等待结果");
        //从future中获取计算结果，该方法是阻塞的，一直等待有结果返回后才结束
        log.debug("结果是{}", future.get());
    }
}
