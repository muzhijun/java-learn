package com.netty.example;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;

/*
【零拷贝】的体现之一，就好比截取了原始 ByteBuf 所有内容，并且没有 max capacity 的限制，也是与原始 ByteBuf 使用同一块底层内存，只是读写指针是独立的
 */
public class DuplicateDemo {

    public static void main(String[] args) {

        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer();
        buffer.writeBytes(new byte[]{1,2,3,4});

        buffer.readByte();
        System.out.println(ByteBufUtil.prettyHexDump(buffer));

        ByteBuf duplicate = buffer.duplicate();
        System.out.println(ByteBufUtil.prettyHexDump(duplicate));

        buffer.readByte();
        System.out.println(ByteBufUtil.prettyHexDump(duplicate));
        System.out.println(ByteBufUtil.prettyHexDump(buffer));

        //方法会对buffer内容进行深拷贝，生成一个新的ByteBuf对象，读写均不受原始ByteBuf对象影响
        ByteBuf copy = buffer.copy();
        System.out.println(ByteBufUtil.prettyHexDump(copy));
    }
}
