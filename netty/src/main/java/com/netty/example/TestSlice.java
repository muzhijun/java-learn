package com.netty.example;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import static com.netty.example.TestByteBuf.log;

public class TestSlice {

    public static void main(String[] args) {

        /**
         * slice 对原始 ByteBuf 进行切片成多个 ByteBuf，切片后的 ByteBuf 并没有发生内存复制，
         * 还是使用原始 ByteBuf 的内存，切片后的 ByteBuf 维护独立的 read，write 指针
         */

        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer(10);
        System.out.println(byteBuf.getClass());
        byteBuf.writeBytes(new byte[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'});
        log(byteBuf);

        ByteBuf f1 = byteBuf.slice(0, 5);
        //手动将 ByteBuf 的引用加1
        f1.retain();
        ByteBuf f2 = byteBuf.slice(5, 5);

        log(f1);
        log(f2);

        //修改slice后的内容
        f1.setByte(0, 'b');

        System.out.println("==========================");
        //ByteBuf 的引用减1，如果后续 ByteBuf 没有被其他地方引用，则继续调用改 ByteBuf 会出错
        byteBuf.release();
        log(f1);
        log(byteBuf);
    }
}
