package com.netty.example;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Server {

    public static void main(String[] args) {

        //创建启动器
        new ServerBootstrap()
                //添加 eventloop
                .group(new NioEventLoopGroup())
                //选择服务端 channel 实现
                .channel(NioServerSocketChannel.class)
                //添加处理器
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        //添加解码器
                        nioSocketChannel.pipeline().addLast(new StringDecoder());
                        //添加自定义处理器
                        nioSocketChannel.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            //从 channel 中读取数据并输出
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                log.debug("接收到消息：{}", msg);
                            }
                        });
                    }
                })
                //绑定端口
                .bind(8080);
    }
}
