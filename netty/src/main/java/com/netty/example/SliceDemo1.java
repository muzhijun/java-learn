package com.netty.example;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.ByteBufUtil;

public class SliceDemo1 {
    public static void main(String[] args) {

        ByteBuf origin = ByteBufAllocator.DEFAULT.buffer(10);
        origin.writeBytes(new byte[]{1, 2, 3, 4});
        origin.readByte();//每进行一次 read 操作，position 位置都会向后移动一位
        System.out.println(ByteBufUtil.prettyHexDump(origin));

        /*
        slice
            原始ByteBuf对象position发生变化，slice不会收到影响，因为slice有独立的读写指针
            slice对内容进行调整，原始ByteBuf对象内容也会随之发生变化，因为二者使用同一块内存地址
         */
        ByteBuf slice = origin.slice();
        System.out.println(ByteBufUtil.prettyHexDump(slice));

        slice.setByte(2, 5);
        System.out.println(ByteBufUtil.prettyHexDump(slice));

        System.out.println(ByteBufUtil.prettyHexDump(origin));

        System.out.println(ByteBufUtil.prettyHexDump(slice));

    }
}
