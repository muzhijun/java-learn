package com.netty.example;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LoggingHandler;

public class TestLengthFieldBasedFrameDecoder {

    public static void main(String[] args) {

        /**
         * maxFrameLength: 最大帧长度
         * lengthFieldOffset: 内容长度偏移量 记录长度之前的内容长度
         * lengthFieldLength: length的字节长度
         * lengthAdjustment: 从长度位置开始调整的长度
         * initialBytesToStrip: 从内容往前丢弃长度
         */
        EmbeddedChannel channel = new EmbeddedChannel(
                new LengthFieldBasedFrameDecoder(
                        1024, 0, 4, 1,5),
                new LoggingHandler()
        );

        ByteBuf buffer = ByteBufAllocator.DEFAULT.buffer();
        writeContent(buffer, "hello world");
        writeContent(buffer, "my name is lilei");
        channel.writeInbound(buffer);
    }

    /**
     * 向buffer写入内容
     * @param buffer
     * @param content
     */
    private static void writeContent(ByteBuf buffer, String content) {
        buffer.writeInt(content.getBytes().length);//写入内容的字节长度
        buffer.writeByte(1);
        buffer.writeBytes(content.getBytes());//写入内容
    }
}
