package com.netty.example;

import io.netty.channel.EventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.DefaultPromise;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;

@Slf4j
public class TestNettyPromise {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        EventLoop eventLoop = new NioEventLoopGroup().next();
        DefaultPromise<Integer> promise = new DefaultPromise<Integer>(eventLoop);

        new Thread(()->{
            try {
                log.debug("开始计算...");
                int a = 1/0;
                Thread.sleep(1000);
                promise.setSuccess(100);
            } catch (Exception e) {
                promise.setFailure(e);
            }
        }).start();

        log.debug("计算结果:{}", promise.get());
    }
}
