package com.netty.example;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestEventLoop {

    public static void main(String[] args) {

        //使用next方法获取下一个事件循环组，获取完成后重新从第一个获取
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup(2);
        System.out.println(eventLoopGroup.next());//获取下一个事件循环组
        System.out.println(eventLoopGroup.next());//获取下一个事件循环组
        System.out.println(eventLoopGroup.next());//获取下一个事件循环组
        System.out.println(eventLoopGroup.next());//获取下一个事件循环组

        //执行普通任务
        /*eventLoopGroup.submit(()->{
            log.debug("new thread run");
        });
        eventLoopGroup.execute(()->{
            log.debug("new thread run");
        });*/

        //执行定时任务
        /*eventLoopGroup.scheduleAtFixedRate(()->{
            log.debug("scheduele thread run");
        }, 0, 1, TimeUnit.SECONDS);*/

        log.debug("main thread run");
    }
}
