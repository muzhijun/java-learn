package com.netty.example;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import static io.netty.buffer.ByteBufUtil.appendPrettyHexDump;
import static io.netty.util.internal.StringUtil.NEWLINE;

public class TestByteBuf {

    public static void main(String[] args) {
        //创建ByteBuf，默认长度256，容量不足自动扩容，每次扩容为原容量*2
        /*
        默认创建的是池化的基于直接内存的 ByteBuf 如果想要创建池化的基于堆内存的 ByteBuf，可以使用：
            ByteBufAllocator.DEFAULT.heapBuffer()
            ByteBufAllocator.DEFAULT.directBuffer() 创建池化的基于直接内存的方式
         */
        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.directBuffer();
        log(byteBuf);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 300; i++) {
            sb.append("a");
        }
        //向ByteBuf中写入数据
        byteBuf.writeBytes(sb.toString().getBytes());
        log(byteBuf);
    }


    public static void log(ByteBuf buffer) {
        int length = buffer.readableBytes();
        int rows = length / 16 + (length % 15 == 0 ? 0 : 1) + 4;
        StringBuilder buf = new StringBuilder(rows * 80 * 2)
                .append("read index:").append(buffer.readerIndex())
                .append(" write index:").append(buffer.writerIndex())
                .append(" capacity:").append(buffer.capacity())
                .append(NEWLINE);
        appendPrettyHexDump(buf, buffer);
        System.out.println(buf.toString());
    }
}
