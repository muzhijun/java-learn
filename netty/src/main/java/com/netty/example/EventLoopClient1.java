package com.netty.example;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.util.Scanner;

@Slf4j
public class EventLoopClient1 {

    public static void main(String[] args) throws InterruptedException {
        NioEventLoopGroup loopGroup = new NioEventLoopGroup();
        //创建启动器
        ChannelFuture channelFuture = new Bootstrap()
                //添加eventloop
                .group(loopGroup)
                //选择客户端 channel 实现
                .channel(NioSocketChannel.class)
                //添加处理器
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        //添加编码器
                        nioSocketChannel.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));//添加日志输出并指定日志级别为debug
                        nioSocketChannel.pipeline().addLast(new StringEncoder());
                    }
                })
                //连接到服务器
                .connect(new InetSocketAddress("localhost", 8080));

        Channel channel = channelFuture.sync().channel();
        //新创建一个 thread 来完成用户输入的捕获与传输
        new Thread(()->{
            Scanner scanner = new Scanner(System.in);
            while (true){
                String line = scanner.nextLine();
                if ("q".equals(line)) {
                    channel.close();
                    break;
                }
                //发送用户输入的内容
                channel.writeAndFlush(line);
            }
        }, "newThread").start();

        ChannelFuture closeFuture = channel.closeFuture();
        /*
        closeFuture.sync();
        log.debug("执行关闭channel后的逻辑");*/

        closeFuture.addListener((ChannelFutureListener) channelFuture1 -> {
            log.debug("执行关闭channel后的逻辑");
            //客户端优雅地关闭所有资源
            loopGroup.shutdownGracefully();
        });
    }
}
