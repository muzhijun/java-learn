package com.netty.example;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;

@Slf4j
public class EventLoopServer {

    public static void main(String[] args) {

        //在与客户端建立连接后，每个channel将会与EventLoop建立一一绑定的关系，专门处理channel中发生的事件

        /**
         细化：
            1.group(new NioEventLoopGroup())方法可以传入两个参数，指定两个EventLoopGroup分别处理 accept 事件
         和 读写 事件 格式：group(new NioEventLoopGroup(), new NioEventLoopGroup(2))  new NioEventLoopGroup(2) 参数如果不设置，默认设置为系统cpu核心线程数*2
            2.addLast方法在添加自定义处理器时，可手动指定分派的 group
         */

        EventLoopGroup eventLoopGroup = new DefaultEventLoopGroup();

        new ServerBootstrap()
                .group(new NioEventLoopGroup(), new NioEventLoopGroup(2))
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        nioSocketChannel.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                //读取byteBuf中的内容
                                ByteBuf buf = (ByteBuf) msg;
                                log.debug("读取到来自客户端的消息：{}", buf.toString(Charset.defaultCharset()));
                            }
                        }).addLast(eventLoopGroup, "handler", new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                //读取byteBuf中的内容
                                ByteBuf buf = (ByteBuf) msg;
                                log.debug("读取到来自客户端的消息：{}", buf.toString(Charset.defaultCharset()));
                            }
                        });
                    }
                })
                .bind(8080);

    }
}
