package com.netty.example;

import io.netty.channel.DefaultEventLoop;
import io.netty.util.concurrent.DefaultPromise;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;

/*
    异步处理任务
 */
@Slf4j
public class PromiseDemo2 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        DefaultEventLoop eventExecutors = new DefaultEventLoop();
        DefaultPromise<Integer>  promise = new DefaultPromise<>(eventExecutors);

        //为promise添加监听事件
        eventExecutors.execute(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //手动设置
            log.debug("set exception{}", 10);
            RuntimeException runtimeException = new RuntimeException("test runtime exception...");
            promise.setFailure(runtimeException);
        });

        log.debug("start...");
        //获取任务结果，非阻塞，还未产生结果时返回 null
        log.debug("{}", promise.getNow());
        //获取任务结果，阻塞等待
//        promise.get();
        //等待任务结束，如果任务失败，不会抛异常，而是通过 isSuccess 判断
    }
}
