package com.netty.example;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EchoServer {

    public static void main(String[] args) {

        new ServerBootstrap()
                .group(new NioEventLoopGroup())
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel nioSocketChannel) throws Exception {
                        nioSocketChannel.pipeline().addLast(new StringDecoder());
                        nioSocketChannel.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                //接收到来自客户端的消息是已经解码的格式，直接toString()即可获取到字符串
                                log.debug("接收到来自客户端的消息:{}", msg.toString());
                                ByteBuf response = ctx.alloc().buffer();
                                response.writeBytes(msg.toString().getBytes());
                                //将接收到的消息转成ByteBuf传给客户端
                                //消息传给客户端需要将字符串转化为 ByteBuf 格式后写入并发送
                                ctx.writeAndFlush(response);
                            }
                        });
                    }
                })
                .bind(8080);

    }
}
