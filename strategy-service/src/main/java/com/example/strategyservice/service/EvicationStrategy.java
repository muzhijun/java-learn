package com.example.strategyservice.service;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/10 17:41
 */
public interface EvicationStrategy {

    boolean select(String evicationType);

    void evicationCache();
}
