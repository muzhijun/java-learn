package com.example.strategyservice.service;

import com.example.strategyservice.enums.EvicationTypeEnum;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/10 18:03
 */
public class MainService {

    public static void main(String[] args) {
        EvicationStrategyFactory factory = new EvicationStrategyFactory();
        factory.execute(EvicationTypeEnum.LFU.name());
    }
}
