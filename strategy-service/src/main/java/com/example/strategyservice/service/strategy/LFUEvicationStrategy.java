package com.example.strategyservice.service.strategy;

import com.example.strategyservice.enums.EvicationTypeEnum;
import com.example.strategyservice.service.EvicationStrategy;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/10 17:46
 */
@Slf4j
public class LFUEvicationStrategy implements EvicationStrategy {
    
    @Override
    public boolean select(String evicationType) {
        return EvicationTypeEnum.LFU.name().equals(evicationType);
    }

    @Override
    public void evicationCache() {
        log.info("lfu execute...");
    }
}
