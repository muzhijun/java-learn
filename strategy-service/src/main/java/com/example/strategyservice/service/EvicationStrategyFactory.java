package com.example.strategyservice.service;

import com.example.strategyservice.service.strategy.FIFOEvicationStrategy;
import com.example.strategyservice.service.strategy.LFUEvicationStrategy;
import com.example.strategyservice.service.strategy.LRUEvicationStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/10 17:47
 */
public class EvicationStrategyFactory {

    private static final List<EvicationStrategy> initialStrategies = new ArrayList<>();

    static {
        initialStrategies.add(new FIFOEvicationStrategy());
        initialStrategies.add(new LRUEvicationStrategy());
        initialStrategies.add(new LFUEvicationStrategy());
    }

    /**
     * 匹配策略，执行
     *
     * @param evicationType
     */
    public void execute(String evicationType) {
        initialStrategies.forEach(evicationStrategy -> {
            if (evicationStrategy.select(evicationType)) {
                evicationStrategy.evicationCache();
            }
        });
    }

}
