package com.example.strategyservice.enums;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/10 17:43
 */
public enum EvicationTypeEnum {

    FIFO,
    LRU,
    LFU;
}
