package com.ljj.redis.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

public class JedisDemo {

    private static Jedis client;
    private static final String HOST = "47.100.203.208";
    private static final Integer PORT = 6379;

    public static void main(String[] args) {
        client = new Jedis(HOST, PORT);
//        client.auth("ljj123456");

//        M1();
//        M2();
//        M3();
//        testSetnx();
        client.flushDB();
        System.out.println(client.incr("a"));
        System.out.println(client.incr("a"));

    }

    private static void M1() {
        System.out.println("==================M1=====================");
        //测试前先清空数据
        client.flushDB();
        //简单类型的数据保存
        client.set("a", "a");
        System.out.println(client.get("a"));

        //有过期时间的数据保存
        /*client.setex("b", 2, "b");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(client.get("b"));*/

        //set if not exist 只有当当前键值不存在，才会保存 -用于分布式锁
        client.setnx("c", "c");
        System.out.println(client.get("c"));
    }

    private static void M2() {
        System.out.println("==================M2=====================");
        //测试前先清空数据
        client.flushDB();

        //lpush 将元素放入list的head
        client.lpush("x-list", "a");
        client.lpush("x-list", "b");
        client.lpush("x-list", "c");
        client.lpush("x-list", "d");

        //rpush 将元素放入list的tail
//        client.rpush("x-list", "a");
//        client.rpush("x-list", "b");
//        client.rpush("x-list", "c");
//        client.rpush("x-list", "d");
        System.out.println(client.lrange("x-list", 0, -1));

        //删除列表指定的值 ，count参数为删除的个数（有重复时），后add进去的值先被删，类似于出栈 value代表需要删除的内容
//        client.lrem("x-list", 1, "a");
        //删除区间以外的数据
//        client.ltrim("x-list", 0, 1);
        //修改列表中指定下标的值
//        client.lset("x-list", 0, "lset");
        //读取list的长度
//        System.out.println(client.llen("x-list"));

        //字符串排序需要使用SortingParams先执行alpha()方法，将sortingParams作为参数，返回结果为排好序的list or set
//        SortingParams sortingParams = new SortingParams();
//        sortingParams.alpha();
//        sortingParams.asc();
//        System.out.println(client.sort("x-list", sortingParams));

        //从head开始读取list中的结果 结果读取后不会出栈
        System.out.println(client.lrange("x-list", 0, -1));

        //lpop 从head开始将list中的数据出栈
//        System.out.println(client.lpop("x-list"));
//        System.out.println(client.lpop("x-list"));
//        System.out.println(client.lpop("x-list"));
//        System.out.println(client.lpop("x-list"));

        //rpop 从tail开始将list中的数据出栈
//        System.out.println(client.rpop("x-list"));
//        System.out.println(client.rpop("x-list"));
//        System.out.println(client.rpop("x-list"));
//        System.out.println(client.rpop("x-list"));
    }

    private static void M3() {
        System.out.println("==================M3=====================");
        //测试前先清空数据
        client.flushDB();
        /*
            · 定义的事务处理过程中如果中间出现语法错误，队列销毁
            · 定义的语法不错，指令执行过程中出现错误，正常的操作会执行成功不受影响，出错的操作失败

            watch机制：
                watch 某个字段，只要这个字段的值在事务执行过程中发生了改变，exec执行结果为nil，当前事务内的操作全部失效
                watch需要在multi执行的前面执行 ！！！
         */
        try {
            //开启事务
            Transaction transaction = client.multi();
            transaction.set("t-a", "t-a");
            //discard方法用于取消事务
//            transaction.discard();
//            transaction.set("t-b", null);
            //执行当前事务组内的所有处理
            transaction.exec();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //save 持久化到硬盘
        client.save();
        //bgsave 持久化到硬盘 并不是立即执行，会寻找合适的时间执行 条件在conf文件中配置
        //会立即返回一个success code
        String bgsave = client.bgsave();
        System.out.println(bgsave);
        System.out.println(client.get("t-a"));
        System.out.println(client.get("t-b"));
    }

    private static void testSetnx() {
        System.out.println("==================M3=====================");
        //测试前先清空数据
        client.flushDB();

        System.out.println("开始抢购........");
        new Thread(() -> {
            System.out.println("小王开始抢......");
            long result = client.setnx("rest", "1");
            if (result == 1) {
                client.del("rest");
                System.out.println("小王抢到了......");
            } else {
                System.out.println("小王没抢到......");
            }
        }).start();
        new Thread(() -> {
            System.out.println("小张开始抢......");
            long result = client.setnx("rest", "1");
            if (result == 1) {
                client.del("rest");
                System.out.println("小张抢到了......");
            } else {
                System.out.println("小张没抢到......");
            }
        }).start();
    }
}
