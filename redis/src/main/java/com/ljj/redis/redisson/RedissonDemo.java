package com.ljj.redis.redisson;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class RedissonDemo {

    public static void main(String[] args) {

        // redisson主要是用来做锁相关的控制
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://47.100.203.208:6379");
        RedissonClient redisson = Redisson.create(config);
    }
}
