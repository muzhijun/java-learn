
# Redis主从复制
模拟环境：
>在一台服务器上启动多个redis服务，分别设定不同的端口号

1.修改端口号分别为6379与6380 6379作为主服务器 6380作为从服务器

2.修改从服务器配置文件：
```shell
slaveof host port
masterauth password //如果主服务器有密码，则需要增加此项配置，否则会连接失败
```
修改好配置文件执行启动命令：

```shell
./bin/redis-server ./conf/redis-6379.conf
./bin/redis-server ./conf/redis-6380.conf
```


上面为配置文件启动方式，也可以直接在启动后执行命令进行连接，这里不做说明。

分别启动6379主服务器与6380从服务器，查看启动日志，失败会有信息提示。前台启动可以设置：
>daemonize yes


