package com.ljj.consumer;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class WorkQueue_Consumer2 {

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.100.203.208");
        factory.setPort(5672);
        factory.setUsername("lijunjun");
        factory.setPassword("lijunjun");
        factory.setVirtualHost("/admin");
        //创建连接connection
        Connection connection = factory.newConnection();
        //创建channel
        Channel channel = connection.createChannel();
        //创建queue
        channel.queueDeclare("work_queues", true, false, false, null);
        //消费消息
        Consumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
//                System.out.println("consumerTag: " + consumerTag);
//                System.out.println("exchange: " + envelope.getExchange());
//                System.out.println("RoutingKey: " + envelope.getRoutingKey());
//                System.out.println("properties: " + properties);
                System.out.println("body: " + new String(body));
            }
        };
        channel.basicConsume("work_queues", true, consumer);
    }
}
