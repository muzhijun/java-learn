package com.ljj.consumer;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer_Topic2 {

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.100.203.208");
        factory.setPort(5672);
        factory.setUsername("lijunjun");
        factory.setPassword("lijunjun");
        factory.setVirtualHost("/admin");
        //创建连接connection
        Connection connection = factory.newConnection();
        //创建channel
        Channel channel = connection.createChannel();
        String queue1_name = "test_topic1";
        String queue2_name = "test_topic2";
        //消费消息
        Consumer consumer = new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("body: " + new String(body));
                System.out.println("queue2 已经处理了消息");
            }
        };
        channel.basicConsume(queue2_name, true, consumer);
    }
}
