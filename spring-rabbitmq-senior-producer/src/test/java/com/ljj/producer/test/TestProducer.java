package com.ljj.producer.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-rabbitmq-producer.xml")
public class TestProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /*
    消息发送成功确认
        方式1：java中直接调用setConfirmCallback()方法，实现匿名函数confirm
        方式2：xml文件中配置bean，创建CallBack类继承RabbitTemplate.ConfirmCallback，实现confirm函数
     */
    @Test
    public void testConfirm(){
        //设置confirm模式下的回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             * 成功发送至交换机
             * @param correlationData 配置信息
             * @param b 是否成功收到消息  true-成功 false-失败
             * @param s 失败的原因 失败的情况有内容 成功返回null
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean b, String s) {
                System.out.println("回调函数成功执行了");
                if (b){
                    System.out.println("成功接收到消息" + s);
                } else {
                    System.out.println("接收消息失败" + s);
                }
            }
        });
        rabbitTemplate.convertAndSend("test_exchange_confirm","confirm", "hi RabbitMQ");
    }

    @Test
    public void testReturn(){
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             *
             * @param message 消息体
             * @param i 错误码
             * @param s 错误文本
             * @param s1 交换机
             * @param s2 路由键
             */
            @Override
            public void returnedMessage(Message message, int i, String s, String s1, String s2) {
                System.out.println("return 回调被执行了");

                System.out.println("========================");
                System.out.println("message: " + message);
                System.out.println("replyCode: " + i);
                System.out.println("replyText: " + s);
                System.out.println("exchange: " + s1);
                System.out.println("routingKey: " + s2);
            }
        });
        rabbitTemplate.convertAndSend("test_exchange_confirm","confirm", "hi RabbitMQ");
    }

    @Test
    public void testSendMultiple(){
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("test_exchange_confirm","confirm", "hi RabbitMQ");
        }
    }

    /*
    TTL
        1 统一过期时间
     */
    @Test
    public void testSendTtl(){
        //统一过期时间
        /*for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("test_exchange_ttl","ttl.hehe", "message ttl...");
        }*/

        /*
        其中一种设置消息过期时间的例子
            队列可以不设置过期时间，单独设置消息过期时间也可以实现消息过期自动丢弃
         */
        /*MessageProperties messageProperties = new MessageProperties();
        messageProperties.setExpiration("5000");
        Message message = new Message("5s自动过期消息".getBytes(), messageProperties);
        rabbitTemplate.convertAndSend("testUqueue", message);*/

        //单独过期时间
        /*
            单独过期时间生效必须要保证当前队列处于队列顶部才可以生效，如果 处于其他位置，设置的过期时间并不会生效，仍然以配置文件中的过期时间为准
         */
        rabbitTemplate.convertAndSend("test_exchange_ttl", "ttl.hehe", "message ttl", new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                //设置消息过期时间
                message.getMessageProperties().setExpiration("5000");
                //返回消息
                return message;
            }
        });

        //不生效的例子 以下代码执行，设置的5s过期时间并不会生效，直到第6条消息处于队列顶端才会生效
        for (int i = 0; i < 10; i++) {
            if (i == 5){
                //定义后置处理器
                MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
                    @Override
                    public Message postProcessMessage(Message message) throws AmqpException {
                        //设置消息过期时间
                        message.getMessageProperties().setExpiration("5000");
                        //返回消息
                        return message;
                    }
                };
                rabbitTemplate.convertAndSend("test_exchange_ttl", "ttl.hehe", "message ttl", messagePostProcessor);
            } else {
                rabbitTemplate.convertAndSend("test_exchange_ttl", "ttl.hehe", "message ttl");
            }
        }
    }

    /**
     * 测试死信队列
     */
    @Test
    public void testDlx(){
        //1 队列超过过期时间
//        rabbitTemplate.convertAndSend("test_exchange_dlx", "test.dlx.haha", "I am dead letter exchange");

        //2 队列长度过长
        /*for (int i = 0; i < 12; i++) {
            rabbitTemplate.convertAndSend("test_exchange_dlx", "test.dlx.haha", "I am dead letter exchange");
        }*/

        rabbitTemplate.convertAndSend("test_exchange_dlx", "test.dlx.haha", "I am dead letter exchange");
    }

    @Test
    public void testDelay(){
        rabbitTemplate.convertAndSend("order_exchange", "order-c.haha", "test delay message...");

        for (int i = 10; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
