package com.ljj.producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class WorkQueue_Producer {

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.100.203.208");
        factory.setPort(5672);
        factory.setUsername("lijunjun");
        factory.setPassword("lijunjun");
        factory.setVirtualHost("/admin");
        //创建连接connection
        Connection connection = factory.newConnection();
        //创建channel
        Channel channel = connection.createChannel();
        //创建queue
        channel.queueDeclare("work_queues", true, false, false, null);
        for (int i = 0; i < 10; i++) {
            //发送消息
            String content = i + "hello rabbitmq~~~";
            channel.basicPublish("", "work_queues", null, content.getBytes());
        }
        //释放资源
        channel.close();
        connection.close();
    }
}
