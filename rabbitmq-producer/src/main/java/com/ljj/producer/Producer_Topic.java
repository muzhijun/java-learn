package com.ljj.producer;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer_Topic {

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.100.203.208");
        factory.setPort(5672);
        factory.setUsername("lijunjun");
        factory.setPassword("lijunjun");
        factory.setVirtualHost("/admin");
        //创建连接connection
        Connection connection = factory.newConnection();
        //创建channel
        Channel channel = connection.createChannel();
        //创建queue
        String exchangeName = "test_topic";
        /*
        exchangeDeclare(String exchange,BuiltinExchangeType type,boolean durable,boolean autoDelete,boolean internal,Map<String, Object> arguments)
        参数：
            1.exchange 交换机名称
            2.type 交换机类型
            3.durable 是否持久化
            4.autoDelete 是否自动删除
            5.internal 内部使用 一般为false
            6.arguments 参数
         */
        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC, true, false,false,null);

        String queue1_name = "test_topic1";
        String queue2_name = "test_topic2";
        /*
        queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete,
                                 Map<String, Object> arguments)
         参数：
            1.queue 队列名称
            2.durable 是否持久化
            3.exclusive 是否独占
            4.autoDelete 是否自动删除
            5.arguments 参数
         */
        channel.queueDeclare(queue1_name, true, false, false, null);
        channel.queueDeclare(queue2_name, true, false, false, null);

        /*
        queueBind(String queue, String exchange, String routingKey)
        参数:
            1.queue 队列名称
            2.exchange 交换机名称
            2.routingKey 路由键 绑定规则
                如果交换机类型为fanout，路由键设置为""
         */
        channel.queueBind(queue1_name, exchangeName, "#.error");
        channel.queueBind(queue1_name, exchangeName, "order.*");
        channel.queueBind(queue2_name, exchangeName, "*.*");

        //发送消息
        String body = "日志信息：张三调用了findAll方法，日志级别：INFO";
        channel.basicPublish(exchangeName, "goods.error", null, body.getBytes());

        //释放资源
        channel.close();
        connection.close();
    }
}
