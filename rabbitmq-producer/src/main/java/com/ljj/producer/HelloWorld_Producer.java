package com.ljj.producer;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class HelloWorld_Producer {

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("47.100.203.208");
        factory.setPort(5672);
        factory.setUsername("lijunjun");
        factory.setPassword("lijunjun");
        factory.setVirtualHost("/admin");
        //创建连接connection
        Connection connection = factory.newConnection();
        //创建channel
        Channel channel = connection.createChannel();
        /**
         * queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String,Object> arguments)
         * queue 队列标识
         * durable 持久化标识
         * exclusive 排他性标识
         * arguments 参数配置
         */
        //创建queue
        channel.queueDeclare("hello_world", true, false, false, null);
        //发送消息
        String content = "hello rabbitmq";
        /**
         * basicPublish(String exchange, String routingKey, AMQP.BasicProperties props, byte[] body)
         * exchange 交换机
         * routingKey 路由
         * props 参数
         * body 发送内容
         */
        channel.basicPublish("", "hello_world", null, content.getBytes());
        //释放资源
        channel.close();
        connection.close();
    }
}
