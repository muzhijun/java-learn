package designmode.abstractfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:25
 */
public class ItalyFactory implements DessertFactory {
    @Override
    public Coffee createCoffee() {
        return new LatteCoffee();
    }

    @Override
    public Dessert createDessert() {
        return new Tiramisu();
    }
}
