package designmode.abstractfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:22
 */
public class MatchaMousse implements Dessert {
    @Override
    public void show() {
        System.out.println("抹茶慕斯蛋糕");
    }
}
