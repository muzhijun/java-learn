package designmode.abstractfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:23
 */
public interface DessertFactory {

    /**
     * 制造咖啡
     *
     * @return
     */
    Coffee createCoffee();

    /**
     * 制造蛋糕
     *
     * @return
     */
    Dessert createDessert();
}
