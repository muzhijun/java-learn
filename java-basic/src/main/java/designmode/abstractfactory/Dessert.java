package designmode.abstractfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:18
 */
public interface Dessert {

    void show();
}
