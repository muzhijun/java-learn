package designmode.abstractfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:21
 */
public class Tiramisu implements Dessert {
    @Override
    public void show() {
        System.out.println("提拉米苏蛋糕");
    }
}
