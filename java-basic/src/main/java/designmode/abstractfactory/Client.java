package designmode.abstractfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:26
 */
public class Client {

    public static void main(String[] args) {
        AmericanFactory americanFactory = new AmericanFactory();
        Coffee coffee = americanFactory.createCoffee();
        Dessert dessert = americanFactory.createDessert();
        System.out.println(coffee.getName());
        dessert.show();

        System.out.println("===========分割线=================");
        ItalyFactory italyFactory = new ItalyFactory();
        Coffee coffee1 = italyFactory.createCoffee();
        Dessert dessert1 = italyFactory.createDessert();
        System.out.println(coffee1.getName());
        dessert1.show();
    }
}
