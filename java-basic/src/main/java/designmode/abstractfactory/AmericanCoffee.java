package designmode.abstractfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:16
 */
public class AmericanCoffee extends Coffee {

    @Override
    public String getName() {
        return "美式咖啡";
    }
}
