package designmode.configfactory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 19:17
 */
public class LatteCoffee extends Coffee {
    @Override
    public String getName() {
        return "拿铁咖啡";
    }
}
