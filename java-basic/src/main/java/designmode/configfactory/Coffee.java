package designmode.configfactory;

/**
 * @author lijunjun
 * @title 咖啡类
 * @date 2021/12/26 15:07
 */
public class Coffee {

    private String name;

    public String getName() {
        return name;
    }

    public void addSugar() {
        System.out.println("加糖");
    }

    public void addMilk() {
        System.out.println("加糖");
    }
}
