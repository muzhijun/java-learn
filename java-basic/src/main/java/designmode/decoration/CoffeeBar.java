package designmode.decoration;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/11 11:07
 */
public class CoffeeBar {

    public static void main(String[] args) {
        Drink order = new LongBlack();
        System.out.println("费用： " + order.cost());
        System.out.println("描述： " + order.getDescription());

        order = new Milk(order);
        System.out.println("加入了一份牛奶 费用： " + order.cost());
        System.out.println("加入了一份牛奶 描述： " + order.getDescription());

        order = new Milk(order);
        System.out.println("加入了一份牛奶 费用： " + order.cost());
        System.out.println("加入了一份牛奶 描述： " + order.getDescription());

    }
}
