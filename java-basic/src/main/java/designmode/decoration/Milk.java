package designmode.decoration;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/11 10:39
 */
public class Milk extends Decorator {

    public Milk(Drink obj) {
        super(obj);
        setDescription(" 加入了牛奶 ");
        setPrice(3.0f);
    }
}
