package designmode.decoration;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/11 10:13
 */
public class Decorator extends Drink {

    private Drink obj;

    public Decorator(Drink obj) {
        this.obj = obj;
    }

    @Override
    public float cost() {
        return getPrice() + obj.cost();
    }

    @Override
    public String getDescription() {
        return obj.getDescription() + super.getDescription();
    }
}
