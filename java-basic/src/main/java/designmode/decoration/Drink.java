package designmode.decoration;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/11 10:09
 */
public abstract class Drink {

    private String description;
    private float price = 0.0f;

    // 费用方法，子类实现
    public abstract float cost();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
