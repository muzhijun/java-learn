package designmode.factorymethod;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 15:11
 */
public class LatteCoffee extends Coffee {

    @Override
    public String getName() {
        return "拿铁咖啡";
    }
}
