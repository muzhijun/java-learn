package designmode.factorymethod;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 15:13
 */
public class LatteCoffeeFactory implements CoffeeFactory {

    @Override
    public Coffee createCoffee() {
        return new LatteCoffee();
    }
}
