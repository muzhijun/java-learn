package designmode.factorymethod;


/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 15:16
 */
public class Client {

    public static void main(String[] args) {
        CoffeeStore store = new CoffeeStore();
        store.setFactory(new LatteCoffeeFactory());// 设置要生产咖啡的工厂
        Coffee coffee = store.orderCoffee();
        System.out.println(coffee.getName());

    }
}
