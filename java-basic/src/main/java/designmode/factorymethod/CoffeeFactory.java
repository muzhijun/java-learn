package designmode.factorymethod;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 15:12
 */
public interface CoffeeFactory {

    /**
     * 创建咖啡
     *
     * @return
     */
    Coffee createCoffee();
}
