package designmode.factorymethod;

/**
 * @author lijunjun
 * @title 咖啡店
 * @date 2021/12/26 15:14
 */
public class CoffeeStore {

    private CoffeeFactory factory;

    public void setFactory(CoffeeFactory factory) {
        this.factory = factory;
    }

    /**
     * 下订单
     *
     * @return
     */
    public Coffee orderCoffee() {
        Coffee coffee = factory.createCoffee();
        coffee.addSugar();
        coffee.addMilk();
        return coffee;
    }
}
