package designmode.factorymethod;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 15:10
 */
public class AmericanCoffee extends Coffee {

    @Override
    public String getName() {
        return "美式咖啡";
    }
}
