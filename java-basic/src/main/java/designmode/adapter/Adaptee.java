package designmode.adapter;

// 网线类 通过网线连接到互联网
public class Adaptee {

    public void connect() {
        System.out.println("通过网线连接到互联网中");
    }
}
