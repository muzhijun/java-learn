package designmode.adapter;

// 适配器
public class Adapter implements NetToUsb {

    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public void handleConnect() {
        adaptee.connect();// 通过网线上网
    }
}
