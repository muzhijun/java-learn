package designmode.adapter;

public class Computer {

    // 上网的具体实现
    public void net(NetToUsb netToUsb) {
        netToUsb.handleConnect();
    }

    public static void main(String[] args) {
        Computer computer = new Computer();// 电脑
        Adaptee adaptee = new Adaptee();// 网线
        Adapter adapter = new Adapter(adaptee);// 适配器
        computer.net(adapter);
    }
}
