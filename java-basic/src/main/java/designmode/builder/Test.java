package designmode.builder;

public class Test {

    public static void main(String[] args) {
        Director director = new Director(new MobaBike());
        Bike bike = director.construct();
        System.out.println(bike.getFrame());
        System.out.println(bike.getSeat());

        director = new Director(new OfoBike());
        bike = director.construct();
        System.out.println(bike.getFrame());
        System.out.println(bike.getSeat());
    }
}
