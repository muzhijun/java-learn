package designmode.builder;

public class MobaBike extends Builder {

    @Override
    public void buildFrame() {
        bike.setFrame("钛合金车架");
    }

    @Override
    public void buildSeat() {
        bike.setSeat("真皮座位");
    }

    @Override
    public Bike createBike() {
        return bike;
    }
}
