package designmode.builder;

public class OfoBike extends Builder {
    @Override
    public void buildFrame() {
        bike.setFrame("铝合金车架");
    }

    @Override
    public void buildSeat() {
        bike.setSeat("塑料座椅");
    }

    @Override
    public Bike createBike() {
        return bike;
    }
}
