package designmode.builder;

/**
 * 抽象建造类
 */
public abstract class Builder {

    /**
     * 设置车架
     */
    public abstract void buildFrame();

    /**
     * 设置座位
     */
    public abstract void buildSeat();

    /**
     * 构造自行车
     *
     * @return
     */
    public abstract Bike createBike();

    protected Bike bike = new Bike();// 初始化自行车类
}
