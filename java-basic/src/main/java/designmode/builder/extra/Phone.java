package designmode.builder.extra;

public class Phone {

    private String cpu;
    private String screen;
    private String ram;
    private String mainboard;

    /**
     * 私有化构造方法，并将参数中build中的参数值赋值给ohone对象
     *
     * @param phoneBuild
     */
    private Phone(PhoneBuild phoneBuild) {
        this.cpu = phoneBuild.cpu;
        this.screen = phoneBuild.screen;
        this.ram = phoneBuild.ram;
        this.mainboard = phoneBuild.mainboard;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "cpu='" + cpu + '\'' +
                ", screen='" + screen + '\'' +
                ", ram='" + ram + '\'' +
                ", mainboard='" + mainboard + '\'' +
                '}';
    }

    public static final class PhoneBuild {

        private String cpu;
        private String screen;
        private String ram;
        private String mainboard;

        public PhoneBuild setCpu(String cpu) {
            this.cpu = cpu;
            return this;
        }

        public PhoneBuild setScreen(String screen) {
            this.screen = screen;
            return this;
        }

        public PhoneBuild setRam(String ram) {
            this.ram = ram;
            return this;
        }

        public PhoneBuild setMainboard(String mainboard) {
            this.mainboard = mainboard;
            return this;
        }

        public Phone build() {
            return new Phone(this);
        }
    }
}
