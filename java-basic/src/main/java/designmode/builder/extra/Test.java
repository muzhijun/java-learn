package designmode.builder.extra;

public class Test {

    public static void main(String[] args) {
        Phone phone = new Phone.PhoneBuild()
                .setCpu("高通骁龙888")
                .setScreen("三星屏幕")
                .setRam("金士顿内存")
                .setMainboard("三星主板")
                .build();
        System.out.println(phone);
    }
}
