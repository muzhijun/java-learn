package designmode.simple;

public class AmericanCoffee extends Coffee {

    @Override
    public String getName() {
        return "美式咖啡";
    }

    @Override
    public String toString() {
        return "AmericanCoffee{}";
    }
}
