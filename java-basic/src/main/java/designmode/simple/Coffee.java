package designmode.simple;

public class Coffee {

    private String name;// 咖啡名称

    public String getName() {
        return name;
    }

    public void addMilk() {
        System.out.println("添加牛奶");
    }

    public void addSurge() {
        System.out.println("添加糖");
    }

}
