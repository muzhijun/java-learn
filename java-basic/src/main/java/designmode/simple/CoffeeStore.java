package designmode.simple;

public class CoffeeStore {

    /**
     * 咖啡店根据类型下单
     *
     * @param type
     * @return
     */
    public Coffee orderCoffee(String type) {
        CoffeeFactory factory = new CoffeeFactory();
        Coffee coffee = factory.createCoffee(type);
        // 可以添加额外的逻辑，如加糖，加奶
        if (coffee != null) {
            coffee.addSurge();
            coffee.addMilk();
        }
        return coffee;
    }
}
