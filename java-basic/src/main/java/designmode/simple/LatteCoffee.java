package designmode.simple;

public class LatteCoffee extends Coffee {

    @Override
    public String getName() {
        return "拿铁咖啡";
    }

    @Override
    public String toString() {
        return "LatteCoffee{}";
    }
}
