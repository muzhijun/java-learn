package designmode.simple;

public class CoffeeFactory {

    /**
     * 这里的方法也可以增加static修饰符，成为静态工厂
     *
     * @param type
     * @return
     */
    public Coffee createCoffee(String type) {
        /**
         * 简单工厂方法违背了开闭原则
         */
        Coffee coffee = null;
        if ("American".equals(type)) {
            coffee = new AmericanCoffee();
        } else if ("latte".equals(type)) {
            coffee = new LatteCoffee();
        }
        return coffee;
    }
}
