package votesystem;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/3/23 21:00
 */
public class VoteSystem {
    private ConcurrentHashMap<String, AtomicInteger> candidates = new ConcurrentHashMap<>();


    public void addCandidate(String name) {
        candidates.put(name, new AtomicInteger(0));
    }

    public void vote(String name) {
        AtomicInteger voteNum = candidates.get(name);
        if (voteNum != null) {
            voteNum.incrementAndGet();
            System.out.println("投票成功");
        } else {
            System.out.println("候选人不存在，请重新输入");
        }
    }

    public void showResult() {
        System.out.println("投票结果如下：");
        for (String name : candidates.keySet()) {
            AtomicInteger count = candidates.get(name);
            System.out.println(name + "：" + count.get() + " 票");
        }
    }

    public static void main(String[] args) {
        VoteSystem voteSystem = new VoteSystem();
        voteSystem.addCandidate("候选人1");
        voteSystem.addCandidate("候选人2");
        voteSystem.addCandidate("候选人3");

        // 创建多个投票线程并启动
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                voteSystem.vote("候选人1");
            }).start();
        }

        voteSystem.showResult();
    }
}
