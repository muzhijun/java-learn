package votesystem;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/3/28 11:34
 */
public class SignUpVotingSystem {
    private static final int MAX_TOTAL_VOTES = 100; // 总票数限制
    private static final int MAX_PER_PERSON_VOTES = 10; // 个人票数限制

    private Map<String, Integer> candidates = new HashMap<>(); // 存储候选人名字及其得票数
    private Map<String, Integer> voters = new HashMap<>(); // 存储投票人名字及其投票数

    public void signUp(String name) {
        candidates.put(name, 0);
    }

    public void vote(String voterName, String candidateName) {
        // 检查总票数限制
        if (candidates.values().stream().mapToInt(Integer::intValue).sum() >= MAX_TOTAL_VOTES) {
            System.out.println("无法投票，总票数已满！");
            return;
        }

        // 检查个人票数限制
        int currentVotes = voters.getOrDefault(voterName, 0);
        if (currentVotes >= MAX_PER_PERSON_VOTES) {
            System.out.println("无法投票，个人票数已满！");
            return;
        }

        // 更新候选人得票数
        int currentVotesForCandidate = candidates.getOrDefault(candidateName, 0);
        candidates.put(candidateName, currentVotesForCandidate + 1);

        // 更新投票人投票数
        voters.put(voterName, currentVotes + 1);
    }

    public static void main(String[] args) {
        SignUpVotingSystem system = new SignUpVotingSystem();

        // 报名
        system.signUp("Tom");
        system.signUp("Jerry");
        system.signUp("Alice");
        system.signUp("Bob");

        // 投票
        system.vote("Voter1", "Tom");
        system.vote("Voter2", "Jerry");
        system.vote("Voter3", "Alice");
        system.vote("Voter4", "Bob");

        // 输出投票结果
        for (String candidateName : system.candidates.keySet()) {
            int votes = system.candidates.get(candidateName);
            System.out.println(candidateName + ": " + votes + " 票");
        }
    }


}
