package annotation;

import java.lang.annotation.*;

// Target用来定义注解使用的范围 Method-方法可以使用
// Retention用来定义注解生效的范围 runtime-执行时生效
// Documented表示是否将注解生成在JavaDoc中
// Inherited用来定义注解是否能够被继承后的对象使用
@Target(value = {ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MyAnnotation {

    String[] value();
}
