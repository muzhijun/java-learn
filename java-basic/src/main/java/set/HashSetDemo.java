package set;

import java.util.HashSet;
import java.util.Hashtable;

public class HashSetDemo {

    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add("a");
        hashSet.add("b");
        System.out.println(hashSet);

        Hashtable hashtable = new Hashtable<>();
        hashtable.put("a", "b");
        System.out.println(hashtable);

    }
}
