package random;

import java.util.Random;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/10/4 19:04
 */
public class Client {


    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(generateRandomCode());
        }

    }

    /**
     * 生成6位随机代码
     *
     * @return {@link String}
     */
    private static String generateRandomCode() {
        Random random = new Random();
        int code = random.nextInt(899999) + 100000;
        return String.valueOf(code);
    }
}
