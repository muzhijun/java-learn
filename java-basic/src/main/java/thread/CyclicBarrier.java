package thread;

public class CyclicBarrier {

    public static void main(String[] args) {
        for (int i = 0; i < 7; i++) {
            final int temp = i;
            new Thread(() -> {
                System.out.println(temp);
            }).start();
        }
    }
}
