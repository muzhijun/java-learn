package thread;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Demo01 {

    public static void main(String[] args) {
        // 通过原生的ThreadPoolExecutor来创建线程池
        // 最大线程数该怎么设置？
        // CPU密集型 Runtime.getRuntime().availableProcessors()
        // IO密集型

        // 线程池的学习口诀： 三大方式 七大参数 四种拒绝策略
        System.out.println(Runtime.getRuntime().availableProcessors());
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                2,
                5,
                2,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardPolicy()
        );
        // 拒绝策略的说明 线程池能接受的最大的线程数=maxmum+blockingQueue的大小
        // AbortPolicy 达到能接受的最大线程数后，再进来会直接报错 java.util.concurrent.RejectedExecutionException
        // CallerRunsPolicy 哪里来回到哪里去 本例回到main线程，由main线程来执行
        // DiscardPolicy 不会抛出异常，超过部分会被丢弃
        // DiscardOldestPolicy 会跟首个执行线程尝试争抢


        try {
            for (int i = 1; i <= 9; i++) {
                final int temp = i;
                threadPoolExecutor.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "==>" + temp);
                });
            }
        } finally {
            threadPoolExecutor.shutdown();
        }
    }
}
