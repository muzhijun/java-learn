package thread;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class Test1 {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();

        MyThread myThread = new MyThread();
        FutureTask futureTask = new FutureTask(myThread);

        new Thread(futureTask, "A").start();
    }
}

class MyThread implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        return null;
    }
}
