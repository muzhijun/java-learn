package thread;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(60);

        for (int i = 1; i <= 60; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "===>Go Out");
                countDownLatch.countDown();
            }, String.valueOf(i)).start();
        }

        countDownLatch.await();

        System.out.println("Close the Door");
    }
}
