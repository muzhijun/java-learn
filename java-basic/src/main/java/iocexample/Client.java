package iocexample;

public class Client {

    public static void main(String[] args) {
        Fruit apple = BeanFactory.getInstance("com.ljj.iocexample.Apple");
        if (apple != null)
            apple.eat();
    }
}
