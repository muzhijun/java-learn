package response;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/6/21 21:09
 */
public class Test {

    public static void main(String[] args) {

        List<TestResponse.Option> options = new ArrayList<>();
        options.add(new TestResponse.Option("1"));
        options.add(new TestResponse.Option("2"));
        options.add(new TestResponse.Option("3"));
        List<TestResponse> list = new ArrayList<>();
        list.add(new TestResponse("zhangsan", 1, options));
        list.add(new TestResponse("lisi", 1, options));
        list.add(new TestResponse("wangwu", 1, options));
        list.add(new TestResponse("wangwu", 1, options));
        list.add(new TestResponse("wangwu", 1, options));
        list.add(new TestResponse("wangwu", 1, options));

        List<Integer> sns = new ArrayList<>();
        AtomicInteger sn = new AtomicInteger(1);
        for (TestResponse t : list) {
            for (int i = 0; i < t.getList().size(); i++) {

                if (i%2==0){
                    sns.add(sn.getAndIncrement());
                } else {
                    sns.add(sn.getAndIncrement());
                }
            }
        }
        sns.forEach(System.out::println);
    }
}
