package response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/6/21 21:07
 */
@Data
@AllArgsConstructor
public class TestResponse {

    private String name;
    private int sn;

    private List<Option> list;

    @Data
    @AllArgsConstructor
    public static class Option{
        private String name;
    }
}
