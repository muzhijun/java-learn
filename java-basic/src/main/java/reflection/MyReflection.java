package reflection;

public class MyReflection {

    public static void main(String[] args) {
        Person person = new Person("张三");
        System.out.println(person.name);

        //方式一
        try {
            Class aClass = Class.forName("com.ljj.reflection.Person");
            System.out.println(aClass.hashCode());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //方式二
        Class aClass = person.getClass();
        System.out.println(aClass.hashCode());

        //方式三
        Class<Person> personClass = Person.class;
        System.out.println(personClass.hashCode());
    }


}

class Person {
    String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }
}
