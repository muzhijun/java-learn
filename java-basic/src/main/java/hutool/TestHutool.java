package hutool;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;

import java.util.Date;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/6 09:48
 */
public class TestHutool {

    public static void main(String[] args) {
        // 数字格式化
        System.out.println(NumberUtil.roundStr("1239.44938", 2));

        // 日期计算 当前日期减去两个月
        System.out.println(DateUtil.offset(new Date(), DateField.MONTH, -2));
        System.out.println(DateUtil.offset(new Date(), DateField.DAY_OF_MONTH, -2));
        System.out.println(DateUtil.offset(new Date(), DateField.DAY_OF_YEAR, -2));
        System.out.println(DateUtil.offset(new Date(), DateField.DAY_OF_WEEK, -2));
        System.out.println(DateUtil.offset(new Date(), DateField.DAY_OF_WEEK_IN_MONTH, -2));
    }
}
