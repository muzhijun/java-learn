import analysesql.SqlParserUtils;
import analysesql.TableField;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.CaseUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/11 17:09
 */
@Slf4j
public class Demo {

    public static void main(String[] args) throws Exception {

        //String test = "name  nam";
        //String[] strings = test.split(" ");
        //List<String> collect = Arrays.stream(strings).filter(StringUtils::isNotBlank).collect(Collectors.toList());
        //System.out.println(collect);

        //String test = "alias_name";
        //System.out.println(CaseUtils.toCamelCase(test, false, '_'));

        //analyse();
        String sql = "  select user_name, birth_day birthDay, email_no as emailNo from test_table where id = ${id}";
        //String sql = "  select user_name, birth_day birthDay, email_no as emailNo from test_table where id = '' and name = '' and email_no = ''";
        //String sql = "select t2.b,t3.c from table2 t2,table3 t3 where t2.b = t3.c";
        //int where = sql.indexOf("where");
        //System.out.println(where);
        //System.out.println(sql.substring(sql.indexOf("where") + 5));

        //List<SelectField> selectFields = SqlParserUtils.select_items(sql);
        //log.info("{}", selectFields.size());
        //
        //List<String> selectTables = SqlParserUtils.select_table(sql);
        //log.info("{}", selectTables.size());

        //String whereStr = SqlParserUtils.select_where(sql);
        String whereStr = SqlParserUtils.getCloumnNames(sql);
        log.info("{}", whereStr);

        //List<Map<String, Object>> parseWhere = SqlParserUtils.parseWhere(sql);
        //log.info("{}", parseWhere);
    }


    private static void analyse() throws Exception {
        // 解析前先执行sql，判断sql是否符合规范

        String sql = "  select user_name, birth_day birthDay, email_no as emailNo from  ";
        // 只允许使用select
        sql = sql.trim();
        // 校验查询语句
        if (!StringUtils.equals("SELECT", sql.substring(0, 6).toUpperCase())) {
            throw new Exception("不支持的类型");
        }
        String output = getContentWithCharacter(sql, "select", "from", 6);
        System.out.println(output);

        // 解析每个字段
        if (StringUtils.isBlank(output) || StringUtils.isBlank(output.trim())) {
            throw new Exception("查询项不可为空");
        }
        if (StringUtils.equals("*", output)) {
            // todo 连接数据库，查询所有字段
        } else {
            String[] outputArr = output.split(",");
            List<TableField> tableFields = new ArrayList<>();
            // name name   name as name  name
            TableField tableField;
            for (int i = 0; i < outputArr.length; i++) {
                tableField = new TableField();
                String content = outputArr[i].trim();
                if (content.contains("as")) {
                    String[] as = content.split("as");
                    // 数据库字段
                    String fieldName = as[0].trim();
                    tableField.setFieldName(fieldName);
                    // 别名
                    String aliasName = as[1].trim();
                    tableField.setAliasName(aliasName);
                } else {
                    String[] contentArr = content.split(" ");
                    List<String> collect = Arrays.stream(contentArr).filter(StringUtils::isNotBlank).collect(Collectors.toList());
                    if (!CollectionUtils.isEmpty(collect) && collect.size() == 1) {
                        String fieldName = collect.get(0).trim();
                        tableField.setFieldName(fieldName);
                        String aliasName = CaseUtils.toCamelCase(collect.get(0), false, '_');
                        tableField.setAliasName(aliasName);
                    } else if (!CollectionUtils.isEmpty(collect) && collect.size() == 2) {
                        String fieldName = collect.get(0).trim();
                        tableField.setFieldName(fieldName);
                        String aliasName = collect.get(1).trim();
                        tableField.setAliasName(aliasName);
                    } else {
                        throw new Exception("格式有误，请确认sql");
                    }
                }
                tableFields.add(tableField);
            }
            for (TableField field : tableFields) {
                System.out.println(field);
            }
        }
    }

    /**
     * 截取两个字符串中间的文本内容
     *
     * @param content     原文本内容
     * @param startStr    开始字符串
     * @param endStr      结束字符串
     * @param beginLength 开始字符长度
     * @return 截取之后的内容
     */
    private static String getContentWithCharacter(String content, String startStr, String endStr, int beginLength) {
        int begin = content.indexOf(startStr);
        int last = content.indexOf(endStr);
        return content.substring(begin + beginLength, last);
    }

}
