package mode.test;

public class Test {

    public static void main(String[] args) {
//        SingletonDemo singletonDemo = SingletonDemo.getInstance();
//        SingletonDemo singletonDemo1 = SingletonDemo.getInstance();
//        System.out.println(singletonDemo);
//        System.out.println(singletonDemo1);
//        System.out.println(singletonDemo1 == singletonDemo);

        Sheep sheep = new Sheep("小样", 12, "白色");
        Sheep sheep2 = (Sheep) sheep.clone();
        Sheep sheep3 = (Sheep) sheep.clone();
        Sheep sheep4 = (Sheep) sheep.clone();
        System.out.println(sheep.toString());
        System.out.println(sheep2.toString());
        System.out.println(sheep3.toString());
        System.out.println(sheep4.toString());

        /*
         位运算：
            1.正整数左移，左侧补0
            2.正整数右移，右侧补0
            3.负整数左移，右侧补1
            3.负整数右移，左侧补1
            4.负整数无符号右移，左侧补0，转化为正整数
         */

        System.out.println(1 << 4);// 左移
        System.out.println(100 >> 2);// 右移
        System.out.println(-5 << 3);
    }
}
