package mode.simplefactory;

public class CarFactory {

    public static Car getWuling() {
        return new Wuling();
    }

    public static Car getTesla() {
        return new Tesla();
    }
}
