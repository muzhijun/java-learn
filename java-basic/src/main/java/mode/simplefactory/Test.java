package mode.simplefactory;

public class Test {
    
    public static void main(String[] args) {
        Car wuling = CarFactory.getWuling();
        Car tesla = CarFactory.getTesla();
        wuling.name();
        tesla.name();
    }
}
