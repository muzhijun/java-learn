package mode.factorymethod;

public class Consumer {

    public static void main(String[] args) {

        /*
        工厂方法模式，工厂的工厂
         */
        WulingFactory wulingFactory = new WulingFactory();
        Car wuling = wulingFactory.getCar();
        wuling.name();

        TeslaFactory teslaFactory = new TeslaFactory();
        Car tesla = teslaFactory.getCar();
        tesla.name();
    }
}
