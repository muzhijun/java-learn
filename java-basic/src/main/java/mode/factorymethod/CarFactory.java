package mode.factorymethod;

public interface CarFactory {

    Car getCar();
}
