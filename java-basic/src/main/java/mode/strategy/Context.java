package mode.strategy;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 21:24
 */
public class Context {

    Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    //上下文接口
    public void contextInterface() {
        strategy.algorithmInterface();
    }
}
