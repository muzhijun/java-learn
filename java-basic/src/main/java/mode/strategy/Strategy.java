package mode.strategy;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 21:23
 */
public abstract class Strategy {

    // 算法方法
    public abstract void algorithmInterface();
}
