package mode.strategy;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 21:25
 */
public class Client {

    public static void main(String[] args) {
        Context context = new Context(new ConcreteStrategyA());
        context.contextInterface();
    }

}
