package mode.strategy;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 21:25
 */
public class ConcreteStrategyA extends Strategy {
    @Override
    public void algorithmInterface() {
        System.out.println("使用策略A");
    }
}
