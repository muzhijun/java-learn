package mode.singleton;

/*
单例模式：
    1.懒汉式
 */
public class SingletonDemo {

    /*
//    懒汉式
    private static SingletonDemo instance;
    private SingletonDemo() {
    }
    public static SingletonDemo getInstance() {
        if (instance == null) {
            instance = new SingletonDemo();
        }
        return instance;
    }*/

    /*
    懒汉式线程安全
    private static SingletonDemo instance;
    private SingletonDemo() {
    }
    // 通过加关键字synchronized来保证线程安全-饿汉式
    public synchronized static SingletonDemo getInstance() {
        if (instance == null) {
            instance = new SingletonDemo();
        }
        return instance;
    }*/

    /*
    // 双重检查
    private volatile static SingletonDemo instance;// 声明为volatile，保证变量的可见行，禁止指令重排

    private SingletonDemo() {
    }

    public static SingletonDemo getInstance() {
        if (instance == null) {// single check
            synchronized (SingletonDemo.class) {
                if (instance == null) {// double check
                    instance = new SingletonDemo();
                }
            }
        }
        return instance;
    }*/

    /*// 饿汉式
    private static final SingletonDemo instance = new SingletonDemo();

    private SingletonDemo() {
    }

    public static SingletonDemo getInstance() {
        return instance;
    }*/

    // 静态内部类方式，仍然属于懒汉式
    private static class SingleHolder {
        private static final SingletonDemo INSTANCE = new SingletonDemo();
    }

    private SingletonDemo() {
    }

    public static final SingletonDemo getInstance() {
        return SingleHolder.INSTANCE;
    }

    // 还可以通过枚举类型进行实现

}
