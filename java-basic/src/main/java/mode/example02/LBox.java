package mode.example02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/9 16:06
 */
public class LBox extends AbstractBox {

    @Override
    public String getShape() {
        return "L";
    }
}
