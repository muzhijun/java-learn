package mode.example02;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/9 16:07
 */
public class BoxFactory {

    // 使用 ReentrantLock
    private static final ReentrantLock lock = new ReentrantLock();

    private static BoxFactory instance;

    private static Map<String, AbstractBox> map;

    private BoxFactory() {
        map = new HashMap<>();
        map.put("I", new IBox());
        map.put("J", new JBox());
        map.put("L", new LBox());
    }

    public static BoxFactory getInstance() {
        // 使用 RetreenLock
        lock.lock();
        try {
            if (instance == null) {
                instance = new BoxFactory();
            }
        } finally {
            lock.unlock();
        }
        return instance;
        //return BoxFactoryHolder.INSTANCE;
    }

    //private static class BoxFactoryHolder {
    //    private static final BoxFactory INSTANCE = new BoxFactory();
    //}

    public AbstractBox getBox(String key) {
        return map.get(key);
    }

}
