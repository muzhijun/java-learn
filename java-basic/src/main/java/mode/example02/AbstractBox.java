package mode.example02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/9 16:02
 */
public abstract class AbstractBox {

    public abstract String getShape();

    public void display(String color) {
        System.out.println("方块的形状是" + getShape() + " 颜色是" + color);
    }

}
