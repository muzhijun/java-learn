package mode.example02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/9 16:07
 */
public class JBox extends AbstractBox {

    @Override
    public String getShape() {
        return "J";
    }
}
