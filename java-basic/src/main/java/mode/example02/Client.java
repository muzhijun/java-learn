package mode.example02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/9 16:10
 */
public class Client {

    public static void main(String[] args) {
        BoxFactory boxFactory = BoxFactory.getInstance();
        AbstractBox box = boxFactory.getBox("I");
        box.display("红色");

        AbstractBox box2 = boxFactory.getBox("I");
        box2.display("绿色");

        System.out.println(box == box2);
        System.out.println(box.equals(box2));

    }
}
