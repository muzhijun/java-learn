package mode.example01;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/8 21:29
 */
public interface Brand {

    void open();

    void close();

    void call();
}
