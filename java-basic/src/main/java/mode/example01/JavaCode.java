package mode.example01;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/9 11:46
 */
public class JavaCode extends CodeDecorator {

    public JavaCode(ICode code) {
        super(code);
    }

    @Override
    public void code() {
        super.code();
        System.out.println("Java程序员写Java代码");
    }
}
