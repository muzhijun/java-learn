package mode.example01;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/8 21:34
 */
public class Xiaomi implements Brand {

    @Override
    public void open() {
        System.out.println("小米手机开机");
    }

    @Override
    public void close() {
        System.out.println("小米手机关机");
    }

    @Override
    public void call() {
        System.out.println("小米手机打电话");
    }
}
