package mode.example01;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/9 11:45
 */
public abstract class CodeDecorator {

    protected ICode code;

    public CodeDecorator(ICode code) {
        this.code = code;
    }

    public void code() {
        this.code.code();
    }
}
