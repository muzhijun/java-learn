package mode.example01;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/8 21:33
 */
public class Client {

    public static void main(String[] args) {
        // 桥接模式
        //Phone phone = new FoldedPhone(new Xiaomi());
        //phone.open();
        //phone.call();
        //phone.close();


        // 装饰者模式
        JavaCode javaCode = new JavaCode(new Coder());
        javaCode.code();
    }
}
