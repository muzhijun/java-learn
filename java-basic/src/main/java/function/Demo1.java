package function;

import java.util.function.Predicate;

public class Demo1 {

    public static void main(String[] args) {

        /*Predicate<String> predicate = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return false;
            }
        };*/

        Predicate<String> predicate = str -> {
            return false;
        };
    }
}
