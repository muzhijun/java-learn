package extra;

public class TestClassLoader {

    public static void main(String[] args) {
        ClassLoader classLoader = TestClassLoader.class.getClassLoader();
        try {
            classLoader.loadClass("com.ljj.extra.TestClassLoader");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
//        ClassLoader parent = classLoader.getParent();
//        System.out.println(parent == null);
//        System.out.println(parent);
//        System.out.println(parent.getParent());
    }
}
