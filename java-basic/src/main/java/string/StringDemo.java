package string;

public class StringDemo {

    public static void main(String[] args) {
        /*
        对于String和Integer类型，他们都重写了equals方法，直接比较的值
         */
        String str1 = new String("a");
        String str2 = new String("a");
        boolean flag = str1.equals(str2);
        boolean flag4 = (str1 == str2);
        System.out.println(flag);
        System.out.println(flag4);

        Integer int1 = new Integer(10);
        Integer int2 = new Integer(10);
        boolean flag2 = int1.equals(int2);
        System.out.println(flag2);
        /*
        其他引用类型的equals比较直接使用==进行判断
         */
        Cat cat1 = new Cat("小花");
        Cat cat2 = new Cat("小花");
        boolean flag3 = cat1.equals(cat2);
        System.out.println(flag3);
    }

    static class Cat extends Animal {

        public Cat(String name) {
            this.name = name;
        }

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    abstract static class Animal {
        public void eat() {
            System.out.println("动物吃东西");
        }

    }
}
