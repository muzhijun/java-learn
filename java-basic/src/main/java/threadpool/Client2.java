package threadpool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/10/25 21:45
 */
public class Client2 {

    private static class Task implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            Thread.sleep(2000);
            return 2;
        }
    }

    private static class MyThread extends Thread {
        @Override
        public void run() {
            System.out.println("执行我的线程run方法");
        }
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        //MyThread myThread = new MyThread();
        //myThread.start();
        //
        //new Thread(() -> {
        //    System.out.println("匿名内部类操作run方法");
        //    System.out.println(Thread.currentThread().getName());
        //}, "哈哈线程").start();


        //ExecutorService pool = Executors.newCachedThreadPool();
        //Task task = new Task();
        //Future<String> future = pool.submit(task);


        //FutureTask<Integer> futureTask = new FutureTask<>(task);
        //pool.submit(futureTask);
        //System.out.println(futureTask.get());
        //pool.shutdown();

        ThreadGroup threadGroup = new ThreadGroup("group1") {
            // 继承ThreadGroup并重新定义以下方法
            // 在线程成员抛出unchecked exception
            // 会执行此方法
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println(t.getName() + ": " + e.getMessage());
            }
        };

        Thread thread = new Thread(threadGroup, () -> {
            // 抛出unchecked异常
            throw new RuntimeException("测试异常");
        });
        thread.start();

        Thread thread2 = new Thread(threadGroup, () -> {
            // 抛出unchecked异常
            throw new RuntimeException("测试异常222");
        });
        thread2.start();

    }
}
