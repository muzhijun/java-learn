package threadpool;


/**
 * @author lijunjun
 * @title 描述
 * @date 2022/10/25 10:52
 */
@FunctionalInterface // 拒绝策略
interface RejectPolicy<T> {

    void reject(BlockingQueue<T> queue, T task);
}
