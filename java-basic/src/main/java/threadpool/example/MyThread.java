package threadpool.example;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/10/25 16:07
 */
public class MyThread {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 50000; i++) {
            list.add(i + "");
        }
        multithreadedListProcess(list);
    }

    private static void multithreadedListProcess(List list) {
        // 开始时间
        long start = System.currentTimeMillis();
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        int listSize = list.size();
        //跑批分页大小
        int limitNum = 500;
        //线程数
        int threadNum = listSize % limitNum == 0 ? listSize / limitNum : listSize / limitNum + 1;
        //最大线程数控制
        int maxthreadNum = 5;

        ExecutorService executor = new ThreadPoolExecutor(maxthreadNum, maxthreadNum,
                1, TimeUnit.MINUTES,
                new ArrayBlockingQueue<>(threadNum), Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.CallerRunsPolicy());

        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        //最大并发线程数控制
        final Semaphore semaphore = new Semaphore(maxthreadNum);
        List handleList = null;
        for (int i = 0; i < threadNum; i++) {
            if ((i + 1) == threadNum) {
                int startIndex = i * limitNum;
                int endIndex = list.size();
                handleList = list.subList(startIndex, endIndex);
            } else {
                int startIndex = i * limitNum;
                int endIndex = (i + 1) * limitNum;
                handleList = list.subList(startIndex, endIndex);
            }
            MultiTask task = new MultiTask(handleList, countDownLatch, semaphore);
            executor.execute(task);
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executor.shutdown();
            System.err.println(Thread.currentThread().getName() + " 执行任务消耗了 ：" + (System.currentTimeMillis() - start) + "毫秒");
        }
    }
}
