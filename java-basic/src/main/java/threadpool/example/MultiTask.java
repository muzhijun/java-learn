package threadpool.example;

import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/10/25 16:06
 */
public class MultiTask implements Runnable {
    private List list;//需要导入的集合
    private CountDownLatch countDownLatch;
    //使用信号量来管理释放
    private Semaphore semaphore;

    public MultiTask(List list, CountDownLatch countDownLatch, Semaphore semaphore) {
        this.list = list;
        this.countDownLatch = countDownLatch;
        this.semaphore = semaphore;

    }

    @Override
    public void run() {
        try {
            if (!CollectionUtils.isEmpty(list)) {
                semaphore.acquire();
                //具体的业务-------------------------------------- start
                //具体的业务-------------------------------------- end
                System.out.println(Thread.currentThread().getName() + "在执行业务。。。");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
            //线程任务完成
            countDownLatch.countDown();
        }
    }

}
