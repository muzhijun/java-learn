import java.util.Scanner;

public class Test {

    public static void main(String[] args) {

        R<Integer> r = new R<Integer>();

        /**
         * 有对兔子（岁数是0个月），
         * 三个月后成年，成年后生出一对兔子，
         * 每对成年兔子每个月生一对兔子，
         * 在不考虑死亡的情况下，2年（24个月）后有多少对兔子
         */


//        1 1
//        2 1
//        3 1 + 1
//        4 1 + 1 + 1
//        5 1 + 1 + 1 + 1 + 1
//        6 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1
//        7 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1

        Scanner sc = new Scanner(System.in);
        System.out.println("请输入月份：");
        int Inputmonth = sc.nextInt();
        int i = 1, j = 0, month, x;
        for (month = 1; month < Inputmonth; month++) {
            x = i;
            i = i + j;
            j = x;
        }
        System.out.println("第" + month + "个月有" + 2 * i + "只兔子。");
    }
}
