package volatiletest;

import java.util.concurrent.atomic.AtomicInteger;

public class Demo1 {

    /*private volatile static int num = 0;
    public synchronized static void add(){
        num++;
    }*/

    private volatile static AtomicInteger num = new AtomicInteger();

    public static void add() {
        num.getAndIncrement();
    }

    public static void main(String[] args) {

        for (int i = 1; i <= 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    add();
                }
            }).start();
        }

        // 注意⚠️ 如果使用if，无法保证原子性
        while (Thread.activeCount() > 2) {// main线程 gc线程
            Thread.yield();
        }

        System.out.println("num=" + num);

    }

}
