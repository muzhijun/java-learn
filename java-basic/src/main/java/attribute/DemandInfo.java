package attribute;

import lombok.Getter;
import lombok.Setter;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/17 13:53
 */
@Getter
@Setter
public class DemandInfo {

    private String id;

    private String name;

    private String chainType;
}
