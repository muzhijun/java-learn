package attribute;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/16 22:06
 */
@Slf4j
@Component
public class FullEqualStrategy implements IBaseStrategy<MyContext>{

    @Override
    public boolean match(MyContext contenxt) {
        return MatchTypeEnum.EQUAL.equals(contenxt.getMatchType());
    }

    @Override
    public void execute(MyContext contenxt) {
        // 完全匹配规则
        String property = contenxt.getProperty();
        //String data = contenxt.get();
        // 转化为需求
        //DemandInfo demandInfo = JSON.parseObject(data, new TypeReference<DemandInfo>() {
        //});
        //String matchData = (String) getAttribute(demandInfo, property);

    }

    public static Object getAttribute(Object o,String field) {
        Object r = null;
        try {
            Field f = o.getClass().getDeclaredField(field);
            f.setAccessible(true);
            return  f.get(o);
        } catch (Exception e) {
            log.info("获取属性值失败 error{}", e.getMessage());
            e.printStackTrace();
        }
        return r;
    }
}
