package attribute;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/16 22:05
 */
public interface IBaseStrategy<MyContenxt> {

    /**
     * 匹配
     *
     * @param contenxt
     * @return boolean
     */
    boolean match(MyContenxt contenxt);

    /**
     * 执行
     */
    void execute(MyContenxt contenxt);
}
