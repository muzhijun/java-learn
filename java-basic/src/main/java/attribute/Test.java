package attribute;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/16 21:26
 */
@Slf4j
public class Test {

    public static void main(String[] args) {
        Person person = new Person("张三", 18, "是个大帅哥");
        String name = (String) getAttribute(person, "name1");
        System.out.println("name = " + name);
    }

    public static Object getAttribute(Object o,String field) {
        Object r = null;
        try {
            Field f = o.getClass().getDeclaredField(field);
            f.setAccessible(true);
            return  f.get(o);
        } catch (Exception e) {
            log.info("获取属性值失败 error{}", e.getMessage());
            e.printStackTrace();

        }
        return r;
    }
}
