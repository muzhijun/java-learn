package attribute;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/16 21:24
 */
public class Person {

    private String name ;

    private Integer age ;

    private String profile;

    public Person() {
    }

    public Person(String name, Integer age, String profile) {
        this.name = name;
        this.age = age;
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
