package attribute;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/17 08:35
 */
public class MyContext {

    private String property;

    private Object value;

    private MatchTypeEnum matchType;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public MatchTypeEnum getMatchType() {
        return matchType;
    }

    public void setMatchType(MatchTypeEnum matchType) {
        this.matchType = matchType;
    }

}
