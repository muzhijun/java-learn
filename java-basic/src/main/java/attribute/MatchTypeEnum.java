package attribute;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/9/17 08:36
 */
public enum MatchTypeEnum {

    EQUAL,

    FUZZY;
}
