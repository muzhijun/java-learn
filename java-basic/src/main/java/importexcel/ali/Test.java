package importexcel.ali;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.util.ListUtils;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 22:51
 */
public class Test {

    private static List<DemoData> data() {
        List<DemoData> list = ListUtils.newArrayList();
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setString("字符串" + i);
            data.setDate(new Date());
            data.setDoubleData(0.56);
            list.add(data);
        }
        return list;
    }

    public static void main(String[] args) {
        String filePath = "/Users/lijunjun/Documents/Project/java-learn/";
        String fileName = filePath + "simpleWrite" + System.currentTimeMillis() + ".xlsx";
        File file = new File(fileName);
        EasyExcel.write(fileName, DemoData.class)
                .sheet("模板")
                .doWrite(() -> {
                    // 分页查询数据
                    return data();
                });

    }
}
