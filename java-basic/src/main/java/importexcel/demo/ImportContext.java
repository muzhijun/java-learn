package importexcel.demo;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 23:18
 */
public class ImportContext {

    private ImportStategy stategy;

    public ImportContext(ImportStategy stategy) {
        this.stategy = stategy;
    }

    // 上下文接口
    public void startImport(String filePath) {
        stategy.algorithmImport(filePath);
    }
}
