package importexcel.demo;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 23:22
 */
public class Client {

    public static void main(String[] args) {
        ImportContext importContext = new ImportContext(new ImportStategyA());
        importContext.startImport("");
    }
}
