package importexcel.demo;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 23:16
 */
public abstract class ImportStategy {

    // 导入算法
    abstract void algorithmImport(String filePath);

}
