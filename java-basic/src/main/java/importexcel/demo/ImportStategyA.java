package importexcel.demo;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 23:19
 */
public class ImportStategyA extends ImportStategy {

    /**
     * ImportStategyA的解析逻辑
     *
     * @param filePath 文件路径
     */
    @Override
    void algorithmImport(String filePath) {
        // 下载文件到本地
        // 读取文件
        // 返回读取后的集合
    }
}
