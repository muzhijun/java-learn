package importexcel.poi;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/3/21 22:59
 */
public class Read {

    /**
     * 导入excel表格.返回的是List<Object> 的数据结构,使用反射去赋值.
     *
     * @param file  导入的文件
     * @param clazz 存储数据的实体
     * @param <T>   表示这个方法是泛型方法.
     * @return
     */
    //public static <T> List<T> readExcel(MultipartFile file, Class<T> clazz) {
    //    try {
    //        //最终返回数据
    //        List<T> resultList = new ArrayList<T>();
    //
    //        Workbook workbook = null;
    //        InputStream is = file.getInputStream();
    //        String name = file.getOriginalFilename().toLowerCase();
    //        // 创建excel操作对象
    //        if (name.contains(".xlsx") || name.contains(".xls")) {
    //            //使用工厂方法创建.
    //            workbook = WorkbookFactory.create(is);
    //        }
    //        //得到一个工作表
    //        Sheet sheet = workbook.getSheetAt(0);
    //        //获得数据的总行数
    //        int totalRowNum = sheet.getLastRowNum();
    //        //获得总列数
    //        int cellLength = sheet.getRow(0).getPhysicalNumberOfCells();
    //        //获取表头
    //        Row firstRow = sheet.getRow(0);
    //        //获取反射类的所有字段
    //        Field[] fields = clazz.getDeclaredFields();
    //        //创建一个字段数组,用于和excel行顺序一致.
    //        Field[] newFields = new Field[cellLength];
    //        //获取静态方法.该方法是用于把excel的表头映射成实体字段名.
    //        // convert方法是在反射的类中的静态方法.也防止Excel中的表头
    //        //和反射的类的字段顺序不一致,导致数据读取在不正确的属性上
    //        /**
    //         public static String convert(String str){
    //         switch (str){
    //         case "身份证号码": return  "idNumber";
    //         case "名称":  return "employeeName";
    //         case "年龄":  return "age";
    //         case "出生日期":  return "birthDate";
    //         case "性别":  return "gender";
    //         default: return  str;
    //         }
    //         }
    //         */
    //        Method m = clazz.getDeclaredMethod("convert", String.class);
    //        Object ob = clazz.newInstance();
    //        for (int i = 0; i < cellLength; i++) {
    //            for (Field field : fields) {
    //                Cell cell = firstRow.getCell(i);
    //                //按照excel中的存储存放数组,以便后面遍历excel表格,数据一一对应.
    //                if (m.invoke(ob, getXCellVal(cell)).equals(field.getName())) {
    //                    newFields[i] = field;
    //                }
    //            }
    //        }
    //
    //        //从第x行开始获取
    //        for (int x = 1; x <= totalRowNum; x++) {
    //            T object = clazz.newInstance();
    //            //获得第i行对象
    //            Row row = sheet.getRow(x);
    //            //如果一行里的所有单元格都为空则不放进list里面
    //            int a = 0;
    //            for (int y = 0; y < cellLength; y++) {
    //                if (!(row == null)) {
    //                    Cell cell = row.getCell(y);
    //                    if (cell == null) {
    //                        a++;
    //                    } else {
    //                        Field field = newFields[y];
    //                        String value = getXCellVal(cell);
    //                        if (value != null && !value.equals("")) {
    //                            //给字段设置值.
    //                            setValue(field, value, object);
    //                        }
    //                    }
    //                }
    //            }//for
    //            if (a != cellLength && row != null) {
    //                resultList.add(object);
    //            }
    //        }
    //        return resultList;
    //    } catch (Exception e) {
    //        e.printStackTrace();
    //    }
    //    return null;
    //}

    /**
     * 给字段赋值,判断值的类型,然后转化成实体需要的类型值.
     *
     * @param field  字段
     * @param value  值
     * @param object 对象
     */
    //private static void setValue(Field field, String value, Object object) {
    //    try {
    //        field.setAccessible(true);
    //        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    //        if (field.getGenericType().toString().contains("Integer")) {
    //            field.set(object, Integer.valueOf(value));
    //        } else if (field.getGenericType().toString().contains("String")) {
    //            field.set(object, value);
    //        } else if (field.getGenericType().toString().contains("Date")) {
    //            field.set(object, fmt.parse(value));
    //        }
    //        field.setAccessible(false);
    //    } catch (Exception e) {
    //        e.printStackTrace();
    //    }
    //}

    /**
     * @param cell
     * @return String
     * 获取单元格中的值
     */

    //private static String getXCellVal(Cell cell) {
    //    DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    //    DecimalFormat df = new DecimalFormat("0.0000");
    //    String val = "";
    //    switch (cell.getCellType()) {
    //        case XSSFCell.CELL_TYPE_NUMERIC:
    //            if (DateUtil.isCellDateFormatted(cell)) {
    //                val = fmt.format(cell.getDateCellValue()); //日期型
    //            } else {
    //                val = df.format(cell.getNumericCellValue()); //数字型
    //                // 去掉多余的0，如最后一位是.则去掉
    //                val = val.replaceAll("0+?$", "").replaceAll("[.]$", "");
    //            }
    //            break;
    //        case XSSFCell.CELL_TYPE_STRING: //文本类型
    //            val = cell.getStringCellValue();
    //            break;
    //        case XSSFCell.CELL_TYPE_BOOLEAN: //布尔型
    //            val = String.valueOf(cell.getBooleanCellValue());
    //            break;
    //        case XSSFCell.CELL_TYPE_BLANK: //空白
    //            val = cell.getStringCellValue();
    //            break;
    //        case XSSFCell.CELL_TYPE_ERROR: //错误
    //            val = "";
    //            break;
    //        case XSSFCell.CELL_TYPE_FORMULA: //公式
    //            try {
    //                val = String.valueOf(cell.getStringCellValue());
    //            } catch (IllegalStateException e) {
    //                val = String.valueOf(cell.getNumericCellValue());
    //            }
    //            break;
    //        default:
    //            val = cell.getRichStringCellValue() == null ? null : cell.getRichStringCellValue().toString();
    //    }
    //    return val;
    //}


}
