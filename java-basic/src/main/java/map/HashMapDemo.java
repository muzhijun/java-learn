package map;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class HashMapDemo {

    public static void main(String[] args) throws InterruptedException {
        // 单线程场景下可以使用HashMap，无法保证线程安全
        new Hashtable<>();
        Map<Object, Object> map = new HashMap<>();
        Map<Object, Object> objectMap = Collections.synchronizedMap(map);
        objectMap.put(null, null);
        System.out.println(map);
        System.out.println(objectMap);

        // 多线程环境下使用ConcurrentHashMap
//        Map<Object, Object> concurrentHashMap = new ConcurrentHashMap<>();

    }
}
