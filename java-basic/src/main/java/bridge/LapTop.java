package bridge;

// 笔记本
public class LapTop extends Computer {
    public LapTop(Brand brand) {
        super(brand);
    }

    @Override
    public void info() {
        super.info();
        System.out.print("笔记本");
    }
}
