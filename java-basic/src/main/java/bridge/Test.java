package bridge;

public class Test {

    public static void main(String[] args) {

        Computer computer = new Desktop(new Lenovo());
        computer.info();

        System.out.println("");

        Computer computer1 = new LapTop(new Apple());
        computer1.info();
    }
}
