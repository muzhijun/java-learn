package com.ljj.thread;

import java.util.concurrent.Callable;

/*
callable接口有返回值，支持范型，允许抛出异常，可以获取异常信息
 */
public class TestCallable implements Callable<Integer> {

    /**
     * Computes a result, or throws an exception if unable to do so.
     *
     * @return computed result
     * @throws Exception if unable to compute a result
     */
    @Override
    public Integer call() throws Exception {
        return null;
    }
}
