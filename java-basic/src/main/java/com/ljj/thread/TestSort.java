package com.ljj.thread;

import com.sun.xml.internal.ws.util.StringUtils;

import java.util.Arrays;

public class TestSort {

    private static final String ASC = "asc";
    private static final String DESC = "desc";

    public static void main(String[] args) {
        int[] arr = {1,10,5,9,22,10,14};
        // 冒泡排序算法
        System.out.println(Arrays.toString(sort1(arr,DESC)));
        System.out.println(Arrays.toString(sort1(arr,ASC)));
    }

    /**
     * 冒泡排序算法 根据指定类型返回排好序的数组
     * @param arr
     * @param sortType 排序类型 asc-升序 desc-降序 不传值默认升序
     * @return
     */
    private static int[] sort1(int[] arr, String sortType){
        if (arr.length == 0){
            return arr;
        }
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if (sortType == null || sortType.isEmpty() || sortType.equals(ASC)){
                    if (arr[i] > arr[j]){
                        int temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                } else {
                    if (arr[i] < arr[j]){
                        int temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        }
        return arr;
    }
}
