package com.ljj.thread;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class TestThreadPool {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService pool = Executors.newSingleThreadExecutor();

        /*
            execute方法参数是runnable
         */
        pool.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("I am execute");
            }
        });

        /*
        submit方法可以有三种参数：
            - Runnable接口
            - Callable接口
            - Runnable接口和T
         */
        Future<?> i_am_submit_runnable = pool.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("I am submit runnable");
            }
        });
        pool.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println("I am submit callable");
                return 0;
            }
        });

        Future<String> future = pool.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("I am submit runnable T");
            }
        }, "a");
        System.out.println(future.get());

        /*
        创建线程的7中方式
         */
//        Executors.newSingleThreadExecutor();
//        Executors.newCachedThreadPool();
//        Executors.newFixedThreadPool(3);
//        Executors.newScheduledThreadPool(3);
//        Executors.newSingleThreadScheduledExecutor();
//        Executors.newWorkStealingPool();
//        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor();
        /*
        ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler)
            corePoolSize 核心线程池大小
            maximumPoolSize 最大线程池大小
            keepAliveTime 线程最大空闲时间
            unit 时间单位
            workQueue 线程等待队列
            threadFactory 线程创建工厂
            handler 拒绝策略
         */

        /*int corePoolSize = 2;
        int maximumPoolSize = 4;
        long keepAliveTime = 10;
        TimeUnit unit = TimeUnit.SECONDS;
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(2);
        ThreadFactory threadFactory = new NameThreadFactory();
        RejectedExecutionHandler handler = new MyIgnorePolicy();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                unit,
                workQueue,
                threadFactory,
                handler);
        executor.prestartAllCoreThreads(); // 预启动所有核心线程
        for (int i = 1; i <= 10; i++) {
            MyTask task = new MyTask(String.valueOf(i));
            executor.execute(task);
        }

        try {
            System.in.read(); //阻塞主线程
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        System.out.println("==============分割线=================");
        AtomicInteger integer= new AtomicInteger();
        for (int i = 0; i < 20; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + "开始工作了...");
                boolean set = integer.compareAndSet(0, 1);
                System.out.println(Thread.currentThread().getName() + "开工作完成了，结果是："+set);
            }).start();
        }
        Thread.sleep(10000);
        System.out.println(integer.get());

    }
        /* JVM的组成部分以及作用
        类加载器 java代码转化成字节码
        运行时数据区 字节码加载到内存中
        执行引擎 将字节码翻译成底层操作系统指令
        本地库接口 在执行引擎翻译的过程中会调用本地库接口完成整个程序的功能


        类加载的顺序：
        加载->检查->准备->解析->初始化
        首先将对应的class文件导入，检查class文件的正确性，为静态变量分配内存空间，将常量池中的符号引用转化为直接引用，为静态变量与静态代码块执行初始化工作。
         */
    static class NameThreadFactory implements ThreadFactory {

        private final AtomicInteger mThreadNum = new AtomicInteger(1);

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r, "my-thread-" + mThreadNum.getAndIncrement());
            System.out.println(t.getName() + " has been created");
            return t;
        }
    }

    public static class MyIgnorePolicy implements RejectedExecutionHandler {

        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            doLog(r, e);
        }

        private void doLog(Runnable r, ThreadPoolExecutor e) {
            // 可做日志记录等
            System.err.println( r.toString() + " rejected");
//          System.out.println("completedTaskCount: " + e.getCompletedTaskCount());
        }
    }

    static class MyTask implements Runnable {
        private String name;

        public MyTask(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                System.out.println(this.toString() + " is running!");
                Thread.sleep(3000); //让任务执行慢点
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "MyTask [name=" + name + "]";
        }
    }
}
