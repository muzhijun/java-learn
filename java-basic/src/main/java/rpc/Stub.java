package rpc;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

/**
 * 代理类
 */
public class Stub {

    public static Object getStub(Class clazz) {
        InvocationHandler invocationHandler = new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                Socket s = new Socket("127.0.0.1", 8080);
                ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());

                // 获取类名
                String clazzName = clazz.getName();
                // 获取方法名
                String methodName = method.getName();
                // 获取参数类型
                Class<?>[] parameterTypes = method.getParameterTypes();

                oos.writeUTF(clazzName);// 写入类名
                oos.writeUTF(methodName);// 写入方法名
                oos.writeObject(parameterTypes);// 写入参数类型
                oos.writeObject(args);// 写入参数值
                oos.flush();

                //接收服务端返回的结果,object读入
                ObjectInputStream ois = new ObjectInputStream(s.getInputStream());

                return ois.readObject();//改为返回通用对象
            }
        };
        Object o = Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, invocationHandler);
        return o;

    }
}
