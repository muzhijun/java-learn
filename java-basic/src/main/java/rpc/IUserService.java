package rpc;

public interface IUserService {

    public User findUserById(Integer id);
}
