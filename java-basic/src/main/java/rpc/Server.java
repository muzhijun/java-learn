package rpc;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * 模拟服务端
 */
public class Server {

    private static boolean RUNNING = true;
    private static HashMap<String, Class> registerTable = new HashMap<>();

    static {
        registerTable.put(IUserService.class.getName(), UserServiceImpl.class);//key类型是接口，value是具体实现类才能完成调用
    }

    public static void main(String[] args) throws Exception {
        ServerSocket server = new ServerSocket(8080);
        while (RUNNING) {
            Socket client = server.accept();
            process(client);
            client.close();
        }
        server.close();
    }

    private static void process(Socket socket) throws Exception {
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        //为了适应客户端通用化而做的改动
        String clazzName = ois.readUTF();
        System.out.println("className" + clazzName);
        String methodName = ois.readUTF();
        Class[] parameterTypes = (Class[]) ois.readObject();
        Object[] parameters = (Object[]) ois.readObject();

        Object service = registerTable.get(clazzName).newInstance();
        Method method = service.getClass().getMethod(methodName, parameterTypes);
        Object o = method.invoke(service, parameters);
        oos.writeObject(o);
        oos.flush();
    }
}
