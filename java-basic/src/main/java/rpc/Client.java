package rpc;

/**
 * 模拟客户端
 */
public class Client {

    public static void main(String[] args) {
        IUserService userService = (IUserService) Stub.getStub(IUserService.class);
        User user = userService.findUserById(11);
        System.out.println(user);
    }
}
