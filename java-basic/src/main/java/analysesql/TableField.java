package analysesql;

import lombok.Data;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/12 10:52
 */
@Data
public class TableField {

    /**
     * 是否主键（1：是，0：否）
     */
    private Integer primaryKey;
    /**
     * 数据库表主键id
     */
    private String tableId;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 别名
     */
    private String aliasName;
    /**
     * 字段名称
     */
    private String fieldName;
    /**
     * 字段描述
     */
    private String fieldDesc;
    /**
     * 字段类型
     */
    private String fieldType;
    /**
     * 字段长度
     */
    private Integer fieldLength;
    /**
     * 小数位数
     */
    private Integer fieldDecimalNumber;
    /**
     * 备注
     */
    private String fieldNote;
    /**
     * 默认值
     */
    private String defaultValue;

}
