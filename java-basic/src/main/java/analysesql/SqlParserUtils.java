package analysesql;

import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.util.TablesNamesFinder;
import net.sf.jsqlparser.util.deparser.ExpressionDeParser;

import java.io.StringReader;
import java.util.*;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/12 16:31
 */
@Slf4j
public class SqlParserUtils {

    /**
     * @Description: 查询sql字段
     * @Author: ywj
     * @Param: [sql]
     * @Return: java.util.List<java.lang.String>
     * @Date: 2020/12/25 15:03
     **/
    public static List<SelectField> select_items(String sql)
            throws JSQLParserException {
        CCJSqlParserManager parserManager = new CCJSqlParserManager();
        Select select = (Select) parserManager.parse(new StringReader(sql));
        PlainSelect plain = (PlainSelect) select.getSelectBody();
        List<SelectItem> selectItems = plain.getSelectItems();
        List<SelectField> selectFields = new ArrayList<>();
        if (selectItems != null) {
            for (SelectItem selectitem : selectItems) {
                selectitem.accept(new SelectItemVisitor() {
                    @Override
                    public void visit(AllColumns allColumns) {
                        log.info("allColumns: {}", allColumns);
                    }

                    @Override
                    public void visit(AllTableColumns allTableColumns) {
                        log.info("allTableColumns: {}", allTableColumns);
                    }

                    @Override
                    public void visit(SelectExpressionItem selectExpressionItem) {
                        SelectField selectField = new SelectField();
                        Expression expression = selectExpressionItem.getExpression();
                        String fieldName = expression.toString();
                        selectField.setFieldName(fieldName);
                        if (!Objects.isNull(selectExpressionItem.getAlias())) {
                            selectField.setAliasName(selectExpressionItem.getAlias().getName());
                        }
                        selectFields.add(selectField);
                    }
                });
            }
        }
        return selectFields;
    }

    /**
     * @Description: 查询表名table
     * @Author: ywj
     * @Param: [sql]
     * @Return: java.util.List<java.lang.String>
     * @Date: 2020/12/25 15:04
     **/
    public static List<String> select_table(String sql)
            throws JSQLParserException {
        Statement statement = CCJSqlParserUtil.parse(sql);
        Select selectStatement = (Select) statement;
        TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
        return tablesNamesFinder.getTableList(selectStatement);
    }

    /**
     * @Description: 查询where
     * @Author: ywj
     * @Param: [sql]
     * @Return: java.lang.String
     * @Date: 2020/12/25 15:06
     **/
    public static String select_where(String sql)
            throws JSQLParserException {
        CCJSqlParserManager parserManager = new CCJSqlParserManager();
        Select select = (Select) parserManager.parse(new StringReader(sql));
        PlainSelect plain = (PlainSelect) select.getSelectBody();
        Expression where_expression = plain.getWhere();
        return where_expression.toString();
    }

    /**
     * 获取SQL中的where后面的条件名称
     *
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static String getCloumnNames(String sql) throws JSQLParserException {
        String columnNames = null;
        String allColumnNames = null;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(sql);
        Statement statement = CCJSqlParserUtil.parse(new StringReader(stringBuffer.toString()));
        if (statement instanceof Select) {
            Select istatement = (Select) statement;
            Expression where = ((PlainSelect) istatement.getSelectBody()).getWhere();
            if (null != where) {
                //装载where后面的字段名称并去重
                Set<String> whereSet = new HashSet<>();
                Set<String> sets = getParser(where, whereSet);
                StringBuffer st = new StringBuffer();
                sets.stream().forEach(set -> {
                    st.append(set + ",");
                });
                columnNames = st.toString();
            }
        }
        if (null != columnNames && columnNames != "" && !columnNames.equals("")) {
            allColumnNames = columnNames.substring(0, columnNames.length() - 1);
        }
        return allColumnNames;
    }

    private static Set<String> getParser(Expression expression, Set<String> set) {
        //初始化接受获得的字段信息
        if (expression instanceof BinaryExpression) {
            //获得左边表达式
            Expression leftExpression = ((BinaryExpression) expression).getLeftExpression();
            String operator = ((BinaryExpression) expression).getStringExpression();
            //获得左边表达式为Column对象，则直接获得列名
            if (leftExpression instanceof Column) {
                String columnName = ((Column) leftExpression).getColumnName();
                set.add(columnName);
            } else if (leftExpression instanceof InExpression) {
                parserInExpression(leftExpression, set);
            } else if (leftExpression instanceof IsNullExpression) {
                parserIsNullExpression(leftExpression, set);
            } else if (leftExpression instanceof BinaryExpression) {//递归调用
                getParser(leftExpression, set);
            } else if (expression instanceof Parenthesis) {//递归调用
                Expression expression1 = ((Parenthesis) expression).getExpression();
                getParser(expression1, set);
            }

            //获得右边表达式，并分解
            Expression rightExpression = ((BinaryExpression) expression).getRightExpression();
            if (rightExpression instanceof BinaryExpression) {
                parserBinaryExpression(rightExpression, set);
            } else if (rightExpression instanceof InExpression) {
                parserInExpression(rightExpression, set);
            } else if (rightExpression instanceof IsNullExpression) {
                parserIsNullExpression(rightExpression, set);
            } else if (rightExpression instanceof Parenthesis) {//递归调用
                Expression expression1 = ((Parenthesis) rightExpression).getExpression();
                getParser(expression1, set);
            }
        } else if (expression instanceof InExpression) {
            parserInExpression(expression, set);
        } else if (expression instanceof IsNullExpression) {
            parserIsNullExpression(expression, set);
        } else if (expression instanceof Parenthesis) {//递归调用
            Expression expression1 = ((Parenthesis) expression).getExpression();
            getParser(expression1, set);
        }
        return set;
    }

    /**
     * @Description: 对where条件解析并返回结果
     * @Author: ywj
     * @Param: [sql, metadata:是否开启原数据]
     * @Return: java.util.List<java.lang.Object>
     * @Date: 2020/12/28 17:14
     **/
    public static List<Map<String, Object>> parseWhere(String sql) {
        try {
            Select select = (Select) CCJSqlParserUtil.parse(sql);
            SelectBody selectBody = select.getSelectBody();
            PlainSelect plainSelect = (PlainSelect) selectBody;
            Expression expr = CCJSqlParserUtil.parseCondExpression(plainSelect.getWhere().toString());
            List<Map<String, Object>> arrList = new ArrayList<>();
            expr.accept(new ExpressionDeParser() {
                int depth = 0;

                @Override
                public void visit(Parenthesis parenthesis) {
                    depth++;
                    parenthesis.getExpression().accept(this);
                    depth--;
                }

                @Override
                public void visit(OrExpression orExpression) {
                    visitBinaryExpr(orExpression, "OR");
                }

                @Override
                public void visit(AndExpression andExpression) {
                    visitBinaryExpr(andExpression, "AND");
                }

                private void visitBinaryExpr(BinaryExpression expr, String operator) {
                    Map<String, Object> map = new HashMap<>();
                    if (!(expr.getLeftExpression() instanceof OrExpression)
                            && !(expr.getLeftExpression() instanceof AndExpression)
                            && !(expr.getLeftExpression() instanceof Parenthesis)) {
                        getBuffer();
                    }
                    expr.getLeftExpression().accept(this);
                    map.put("leftExpression", expr.getLeftExpression());
                    map.put("operator", operator);
                    if (!(expr.getRightExpression() instanceof OrExpression)
                            && !(expr.getRightExpression() instanceof AndExpression)
                            && !(expr.getRightExpression() instanceof Parenthesis)) {
                        getBuffer();
                    }
                    expr.getRightExpression().accept(this);
                    map.put("rightExpression", expr.getRightExpression());
                    arrList.add(map);
                }
            });
            return arrList;
        } catch (JSQLParserException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解析in关键字左边的条件
     *
     * @param expression
     */
    public static void parserInExpression(Expression expression, Set<String> set) {
        Expression leftExpression = ((InExpression) expression).getLeftExpression();
        if (leftExpression instanceof Column) {
            String columnName = ((Column) leftExpression).getColumnName();
            set.add(columnName);
        }
    }

    /**
     * 解析is null 和 is not null关键字左边的条件
     *
     * @param expression
     */
    public static void parserIsNullExpression(Expression expression, Set<String> set) {
        Expression leftExpression = ((IsNullExpression) expression).getLeftExpression();
        if (leftExpression instanceof Column) {
            String columnName = ((Column) leftExpression).getColumnName();
            set.add(columnName);
        }
    }

    public static void parserBinaryExpression(Expression expression, Set<String> set) {
        Expression leftExpression = ((BinaryExpression) expression).getLeftExpression();
        if (leftExpression instanceof Column) {
            String columnName = ((Column) leftExpression).getColumnName();
            set.add(columnName);
        }
    }

}
