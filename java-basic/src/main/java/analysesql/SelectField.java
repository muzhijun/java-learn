package analysesql;

import lombok.Getter;
import lombok.Setter;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/13 11:21
 */
@Getter
@Setter
public class SelectField {

    // 字段
    private String fieldName;

    // 别名
    private String aliasName;
}
