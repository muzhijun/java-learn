package com.ljj.shardingjdbc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//如果使用java配置文件版，则需要屏蔽shrdingjdbc的configuration类！！！
//@SpringBootApplication(exclude = SpringBootConfiguration.class)
@SpringBootApplication
@MapperScan(basePackages = "com.ljj.shardingjdbc.dao")
public class ShardingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShardingApplication.class, args);
    }
}
