package com.ljj.shardingjdbc.pojo;

import lombok.Data;

@Data
public class Dict {

    private Long userId;
    private String fullname;
    private String userType;

    @Override
    public String toString() {
        return "Dict{" +
                "userId=" + userId +
                ", fullname='" + fullname + '\'' +
                ", userType='" + userType + '\'' +
                '}';
    }
}
