package com.ljj.shardingjdbc.pojo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Order {

    private Long orderId;
    private BigDecimal price;
    private Long userId;
    private String status;

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", price=" + price +
                ", userId=" + userId +
                ", status='" + status + '\'' +
                '}';
    }
}
