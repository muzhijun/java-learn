package com.ljj.shardingjdbc.pojo;

import lombok.Data;

@Data
public class User {

    private Long userId;
    private String fullname;
    private String userType;

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", fullname='" + fullname + '\'' +
                ", userType='" + userType + '\'' +
                '}';
    }
}
