package com.ljj.shardingjdbc.dao;

import com.ljj.shardingjdbc.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DictDao {

    /**
     * 插入数据字典信息
     * @param dictId
     * @param type
     * @param code
     * @param value
     */
    @Insert("insert into t_dict (dict_id, type, code, value)  values (#{dictId}, #{type}, #{code}, #{value})")
    void insertDictData(@Param("dictId") Long dictId, @Param("type")String type, @Param("code")String code, @Param("value")String value);

    /**
     * 根据dictId删除数据字典信息
     * @param dictId
     */
    @Delete("delete from t_dict where dict_id = #{dictId}")
    void deleteDictData(@Param("dictId")Long dictId);

    /**
     * 根据dictIds查询数据字典信息
     * @param dictIds
     * @return
     */
    @Select("<script>" +
            "select" +
            " * " +
            "from t_dict t " +
            "where " +
            "t.dict_id in " +
            "<foreach collection='dictIds' open='(' separator=',' close=')' item='id'>" +
            "#{id}" +
            "</foreach>" +
            "</script>"
    )
    List<User> selectUserByIds(@Param("dictIds")List<Long> dictIds);
}
