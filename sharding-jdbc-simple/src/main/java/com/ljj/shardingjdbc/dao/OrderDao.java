package com.ljj.shardingjdbc.dao;

import com.ljj.shardingjdbc.pojo.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public interface OrderDao {

    /**
     * 插入订单信息表
     * @param price
     * @param userId
     * @param status
     */
    @Insert("insert into t_order (price, user_id, status)  values (#{price}, #{userId}, #{status})")
    void insertOrderData(@Param("price") BigDecimal price, @Param("userId")long userId, @Param("status")String status);

    /**
     * 根据orderIds查询结果
     * @param orderIds
     * @return
     */
    @Select("<script>" +
            "select" +
            " * " +
            "from t_order t " +
            "where " +
            "t.order_id in " +
            "<foreach collection='orderIds' open='(' separator=',' close=')' item='id'>" +
            "#{id}" +
            "</foreach>" +
            "</script>"
    )
    List<Order> selectOrderByIds(@Param("orderIds")List<Long> orderIds);
}
