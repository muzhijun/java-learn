package com.ljj.shardingjdbc.dao;

import com.ljj.shardingjdbc.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface UserDao {

    /**
     * 插入用户信息表
     * @param userId
     * @param fullname
     */
    @Insert("insert into t_user (user_id, fullname)  values (#{userId}, #{fullname})")
    void insertUserData(@Param("userId") Long userId, @Param("fullname")String fullname);

    /**
     * 根据userIds查询用户信息
     * @param userIds
     * @return
     */
    @Select("<script>" +
            "select" +
            " * " +
            "from t_user t " +
            "where " +
            "t.user_id in " +
            "<foreach collection='userIds' open='(' separator=',' close=')' item='id'>" +
            "#{id}" +
            "</foreach>" +
            "</script>"
    )
    List<User> selectUserByIds(@Param("userIds")List<Long> userIds);

    /**
     * 根据userIds查询用户信息
     * @param userIds
     * @return
     */
    @Select({"<script>",
            " select",
            " * ",
            " from t_user t ,t_dict b",
            " where t.user_type = b.code and t.user_id in",
            "<foreach collection='userIds' item='id' open='(' separator=',' close=')'>",
            "#{id}",
            "</foreach>",
            "</script>"
    })
    List<Map> selectUserInfobyIds(@Param("userIds")List<Long> userIds);
}
