package com.ljj.shardingjdbc.config;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/*
TableShardingAlgorithm 定义分表策略 表名_拼接日期
 */
public class TableShardingAlgorithm implements PreciseShardingAlgorithm<String> {

    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<String> preciseShardingValue) {
        //获取逻辑表名
        String tb_name = preciseShardingValue.getLogicTableName() + "_";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(preciseShardingValue.getValue());
            String year = String.format("%tY", date);
            String mon = String.format("%tm", date);
            String dat = String.format("%td", date);
            //格式：逻辑表名_年月日
            tb_name = tb_name + year + mon + dat;
            System.out.println("tb_name:" + tb_name);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (String each : collection) {
            System.out.println("t_order_:" + each);
            if (each.equals(tb_name)) {
                return each;
            }
        }

        throw new IllegalArgumentException();
    }
}
