package com.ljj.shardingjdbc;

import com.ljj.shardingjdbc.dao.UserDao;
import com.ljj.shardingjdbc.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingApplication.class)
public class TestUserDao {

    @Autowired
    private UserDao userDao;

    @Test
    public void testInsertUserData(){
        for (int i = 1; i <= 10; i++) {
            userDao.insertUserData((long) i, "zhang"+i);
        }
    }

    @Test
    public void testSelectUserByIds(){
        List<User> orderList = userDao.selectUserByIds(Arrays.asList(1L, 5L));
        orderList.forEach((order -> System.out.println(order.toString())));
    }

    @Test
    public void testSelectUserInfobyIds(){
        List<Map> userInfos = userDao.selectUserInfobyIds(Arrays.asList(1L, 5L));
        System.out.println(userInfos);
    }
}
