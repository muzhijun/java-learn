package com.ljj.shardingjdbc;

import com.ljj.shardingjdbc.dao.DictDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingApplication.class)
public class TestDictDao {

    @Autowired
    private DictDao dictDao;

    @Test
    public void testInsertUserData(){
       /* for (int i = 1; i <= 10; i++) {
            dictDao.insertDictData((long) i, "admin", "0", "超级管理员");
        }*/

        dictDao.insertDictData(1L, "user_type", "0", "超级管理员");
        dictDao.insertDictData(2L, "user_type", "1", "二级管理员");
    }

    @Test
    public void deleteDictData(){
        for (int i = 1; i <= 10; i++) {
            dictDao.deleteDictData((long)i);
        }
    }

    @Test
    public void testSelectUserByIds(){
//        List<User> orderList = userDao.selectUserByIds(Arrays.asList(1L, 5L));
//        orderList.forEach((order -> System.out.println(order.toString())));
    }
}
