package com.ljj.shardingjdbc;

import com.ljj.shardingjdbc.dao.OrderDao;
import com.ljj.shardingjdbc.pojo.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShardingApplication.class)
public class TestOrder {

    /*
    配置方式：
        1.使用application.properties文件配置
        2.使用application.yml文件配置
        3.使用自动配置类(java版本)
        4.使用xml配置文件-不推荐！！！
     */

    @Autowired
    private OrderDao orderDao;

    @Test
    public void testInsertOrderData(){
        for (int i = 1; i <= 10; i++) {
            //根据配置文件配置的雪花算法，自动插入到对应的表中
            orderDao.insertOrderData(new BigDecimal(100).add(new BigDecimal(i)), i, "0");
        }
    }

    @Test
    public void testSelectOrderByIds(){
        //sharding-jdbc自动会根据分片策略查找到需要从哪张表去查询
        List<Order> orderList = orderDao.selectOrderByIds(Arrays.asList(667770862341980161L, 667770862853685248L));
        orderList.forEach((order -> System.out.println(order.toString())));
    }
}
