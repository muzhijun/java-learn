package com.ljj.pattern.configfactory.iteratorpattern.example3;

import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 22:01
 */
public class ProductList extends AbstractObjectList {

    public ProductList(List objects) {
        super(objects);
    }

    @Override
    public AbstractIterator createIterator() {
        return new ProductIterator();
    }

    private class ProductIterator implements AbstractIterator {

        private int cursor;// 正向遍历游标
        private int cursor1;// 逆向遍历游标

        public ProductIterator() {
            this.cursor = 0;
            this.cursor1 = objects.size() - 1;
        }

        @Override
        public void next() {
            if (this.cursor < objects.size()) {
                this.cursor++;
            }
        }

        @Override
        public boolean isLast() {
            return this.cursor == objects.size();
        }

        @Override
        public void previous() {
            if (this.cursor1 > -1) {
                this.cursor1--;
            }
        }

        @Override
        public boolean isFirst() {
            return this.cursor1 == -1;
        }

        @Override
        public Object getNextItem() {
            return objects.get(cursor);
        }

        @Override
        public Object getPreviousItem() {
            return objects.get(this.cursor1);
        }
    }
}
