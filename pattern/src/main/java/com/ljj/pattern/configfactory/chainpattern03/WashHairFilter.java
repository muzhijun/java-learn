package com.ljj.pattern.configfactory.chainpattern03;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:43
 */
public class WashHairFilter implements StudyPrepareFilter {

    @Override
    public void doFilter(PreparationList preparationList, FilterChain filterChain) {
        if (preparationList.isWashHair()) {
            System.out.println("洗头");
        }
        filterChain.doFilter(preparationList, filterChain);
    }
}
