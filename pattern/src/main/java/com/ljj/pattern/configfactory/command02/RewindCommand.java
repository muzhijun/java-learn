package com.ljj.pattern.configfactory.command02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:34
 */
public class RewindCommand implements Command {

    private AudioPlayer audioPlayer;

    public RewindCommand(AudioPlayer audioPlayer) {
        this.audioPlayer = audioPlayer;
    }

    @Override
    public void execute() {
        audioPlayer.rewind();
    }
}
