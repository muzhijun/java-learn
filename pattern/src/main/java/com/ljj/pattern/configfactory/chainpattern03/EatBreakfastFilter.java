package com.ljj.pattern.configfactory.chainpattern03;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:43
 */
public class EatBreakfastFilter implements StudyPrepareFilter {

    @Override
    public void doFilter(PreparationList preparationList, FilterChain filterChain) {
        if (preparationList.isHaveBreakfast()) {
            System.out.println("吃早饭");
        }
        filterChain.doFilter(preparationList, filterChain);
    }
}
