package com.ljj.pattern.configfactory.observerpattern.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/23 21:16
 */
public class ConcreteSubject extends Subject {
    @Override
    public void myNotify() {
        //遍历观察者集合，调用每一个观察者的响应方法
        for (Object obs : observers) {
            ((Observer) obs).update();
        }
    }
}
