package com.ljj.pattern.configfactory.builder.example4;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 11:05
 */
public class House {

    private String window;
    private String floor;

    public void apply(WorkBuilder.HouseParam param) {
        this.window = param.window;
        this.floor = param.floor;
    }
    
    @Override
    public String toString() {
        return window + floor;
    }
}
