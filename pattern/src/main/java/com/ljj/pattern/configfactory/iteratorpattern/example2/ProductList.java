package com.ljj.pattern.configfactory.iteratorpattern.example2;

import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 22:01
 */
public class ProductList extends AbstractObjectList {

    public ProductList(List<Object> objects) {
        super(objects);
    }

    @Override
    public AbstractIterator createIterator() {
        return new ProductIterator(this);
    }
}
