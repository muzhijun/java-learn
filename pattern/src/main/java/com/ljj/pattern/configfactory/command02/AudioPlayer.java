package com.ljj.pattern.configfactory.command02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:33
 */
public class AudioPlayer {

    public void play() {
        System.out.println("播放音乐！");
    }

    public void rewind() {
        System.out.println("倒带！");
    }

    public void stop() {
        System.out.println("停止播放！");
    }
}
