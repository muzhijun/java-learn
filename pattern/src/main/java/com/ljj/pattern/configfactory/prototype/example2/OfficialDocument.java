package com.ljj.pattern.configfactory.prototype.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/5 15:44
 */
public interface OfficialDocument extends Cloneable {

    // 接口对 Cloneable 进行实现，表明该类可以被克隆
    // 接口能够对Cloneable进行实现吗？
    // 接口不能实现接口，只能继承接口

    public OfficialDocument clone();

    public void display();
    
}
