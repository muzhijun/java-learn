package com.ljj.pattern.configfactory.builder.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:15
 */
public class Client {

    public static void main(String[] args) {

        System.out.println("准备组装一台电脑");

        // 通过指挥者指挥具体的建造者，这里无需关心具体建造者内部的建造顺序
        Builder builder = new ConcreteBuilder();
        Director director = new Director(builder);
        Product product = director.construct();
        System.out.println(product.toString());


    }
}
