package com.ljj.pattern.configfactory.factory;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 20:15
 */
public class Client {

    public static void main(String[] args) {
        Coffee american = CoffeeFactory.createCoffee("american");
        System.out.println(american.getName());
    }
}
