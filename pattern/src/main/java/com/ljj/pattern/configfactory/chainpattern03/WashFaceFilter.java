package com.ljj.pattern.configfactory.chainpattern03;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:41
 */
public class WashFaceFilter implements StudyPrepareFilter {

    @Override
    public void doFilter(PreparationList preparationList, FilterChain filterChain) {
        if (preparationList.isWashFace()) {
            System.out.println("洗脸");
        }

        filterChain.doFilter(preparationList, filterChain);
    }
}
