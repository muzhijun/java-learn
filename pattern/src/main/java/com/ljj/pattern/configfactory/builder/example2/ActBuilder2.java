package com.ljj.pattern.configfactory.builder.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:41
 */
public abstract class ActBuilder2 {

    protected Actor actor = new Actor();

    public abstract void buildType();

    public abstract void buildSex();

    public abstract void buildFace();

    public abstract void buildCostume();

    public abstract void buildHairstyle();

    public Actor construct() {
        buildType();
        buildSex();
        buildFace();
        buildCostume();
        buildHairstyle();
        return actor;
    }
}
