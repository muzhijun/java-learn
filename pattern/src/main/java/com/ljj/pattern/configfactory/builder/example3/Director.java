package com.ljj.pattern.configfactory.builder.example3;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 10:51
 */
public class Director {

    public House design(Builder builder) {
        builder.makeWindow();
        builder.makeFloor();
        return builder.getHouse();
    }
}
