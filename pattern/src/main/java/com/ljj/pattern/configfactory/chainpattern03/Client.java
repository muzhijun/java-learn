package com.ljj.pattern.configfactory.chainpattern03;

import com.ljj.pattern.configfactory.chainpattern02.Study;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:46
 */
public class Client {

    public static void main(String[] args) {

        // 此种方式为升级版的责任链模式写法，可以在链条中添加新的过滤器，而不需要修改原有的代码 符合开闭原则
        PreparationList preparationList = new PreparationList();
        preparationList.setWashFace(true);
        preparationList.setWashHair(true);
        preparationList.setHaveBreakfast(true);

        StudyPrepareFilter studyPrepareFilter = new WashFaceFilter();
        StudyPrepareFilter studyPrepareFilter1 = new WashHairFilter();
        StudyPrepareFilter studyPrepareFilter2 = new EatBreakfastFilter();

        Study study = new Study();

        FilterChain filterChain = new FilterChain(preparationList, study);
        filterChain.addFilter(studyPrepareFilter);
        filterChain.addFilter(studyPrepareFilter1);
        filterChain.addFilter(studyPrepareFilter2);

        filterChain.doFilter(preparationList, filterChain);
    }
}
