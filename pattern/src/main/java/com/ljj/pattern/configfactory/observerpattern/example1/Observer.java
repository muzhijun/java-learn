package com.ljj.pattern.configfactory.observerpattern.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/23 21:17
 */
public interface Observer {
    
    //声明响应方法  
    public void update();
}
