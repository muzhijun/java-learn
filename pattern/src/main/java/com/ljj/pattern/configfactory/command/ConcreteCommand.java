package com.ljj.pattern.configfactory.command;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:23
 */
public class ConcreteCommand implements ICommand {

    private Receiver receiver;

    public ConcreteCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.action();
    }
}
