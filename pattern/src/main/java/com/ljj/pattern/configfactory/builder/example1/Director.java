package com.ljj.pattern.configfactory.builder.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:12
 */
public class Director {

    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public void setBuilder(Builder builder) {
        this.builder = builder;
    }

    public Product construct() {
        this.builder.buildPartA();
        this.builder.buildPartB();
        this.builder.buildPartC();
        return this.builder.getResult();
    }
}
