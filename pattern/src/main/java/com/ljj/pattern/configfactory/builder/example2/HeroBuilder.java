package com.ljj.pattern.configfactory.builder.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:28
 */
public class HeroBuilder extends ActorBuilder {
    @Override
    public void buildType() {
        this.actor.setType("英雄");
        System.out.println("构造一个英雄角色");
    }

    @Override
    public void buildSex() {
        this.actor.setSex("男");
        System.out.println("确定性别：男");
    }

    @Override
    public void buildFace() {
        this.actor.setFace("国字脸");
        System.out.println("确定脸型：国字脸");
    }

    @Override
    public void buildCostume() {
        this.actor.setCostume("内裤外穿");
        System.out.println("确定服饰：内裤外穿");
    }

    @Override
    public void buildHairstyle() {
        this.actor.setHairstyle("爆炸头");
        System.out.println("确定发型：爆炸头");
    }
}
