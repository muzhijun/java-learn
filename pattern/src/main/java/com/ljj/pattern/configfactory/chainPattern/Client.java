package com.ljj.pattern.configfactory.chainPattern;

/**
 * @author lijunjun
 * @className Client
 * @description Client
 * @date 2022/7/18 14:55
 */
public class Client {

    public static void main(String[] args) {
        Approver zhuren, fudongshizhang, dongshizhang, dongshihui;
        zhuren = new Director("张三");
        fudongshizhang = new VicePresident("李四");
        dongshizhang = new President("王二");
        dongshihui = new Congress("麻子");

        //创建职责链
        zhuren.setSuccessor(fudongshizhang);
        fudongshizhang.setSuccessor(dongshizhang);
        dongshizhang.setSuccessor(dongshihui);

        PurchaseRequest request = new PurchaseRequest(45000, 10001, "购买馒头");
        zhuren.processRequest(request);

        request = new PurchaseRequest(85000, 10002, "购买大米");
        zhuren.processRequest(request);

        request = new PurchaseRequest(145000, 10003, "购买猪肉");
        zhuren.processRequest(request);

        request = new PurchaseRequest(555000, 10004, "购买牛肉");
        zhuren.processRequest(request);

        request = new PurchaseRequest(115000, 10005, "购买鱼");
        zhuren.processRequest(request);

    }
}
