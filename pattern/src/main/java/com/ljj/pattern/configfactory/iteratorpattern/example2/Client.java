package com.ljj.pattern.configfactory.iteratorpattern.example2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 22:11
 */
public class Client {

    public static void main(String[] args) {
        List products = new ArrayList();
        products.add("倚天剑");
        products.add("屠龙刀");
        products.add("断肠草");
        products.add("葵花宝典");
        products.add("四十二章经");

        ProductList productList = new ProductList(products);

        AbstractIterator productListIterator = productList.createIterator();
        while (!productListIterator.isLast()) {
            System.out.println(productListIterator.getNextItem());
            productListIterator.next();
        }

        while (!productListIterator.isFirst()) {
            System.out.println(productListIterator.getPreviousItem());
            productListIterator.previous();
        }


    }
}
