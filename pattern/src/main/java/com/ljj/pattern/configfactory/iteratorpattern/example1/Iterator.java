package com.ljj.pattern.configfactory.iteratorpattern.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 21:41
 */
public interface Iterator {

    void first();

    void next();

    boolean hasNext();

    void last();
}
