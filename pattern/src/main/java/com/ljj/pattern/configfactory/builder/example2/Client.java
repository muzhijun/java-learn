package com.ljj.pattern.configfactory.builder.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:29
 */
public class Client {

    public static void main(String[] args) {
        //ActorController controller = new ActorController();
        //Actor actor = controller.construct(new HeroBuilder());
        //System.out.println(actor.toString());


        ActBuilder2 angleBuild = new AngleBuild();
        System.out.println(angleBuild.construct().toString());
    }
}
