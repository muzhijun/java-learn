package com.ljj.pattern.configfactory.chainpattern02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:03
 */
public class WashHairPrepareFilter extends AbstractPrepareFilter {

    public WashHairPrepareFilter(AbstractPrepareFilter nextFilter) {
        super(nextFilter);
    }

    @Override
    public void prepare(PreparedList preparedList) {
        if (!preparedList.isWashHair()) {
            System.out.println("洗头");
        }
    }
}
