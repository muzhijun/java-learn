package com.ljj.pattern.configfactory.prototype;

import java.io.IOException;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/5 13:55
 */
public class Client {

    public static void main(String[] args) {

        // 实现Cloneable接口，重写clone方法实现浅克隆
        // 实现Serializable接口，通过流方式实现深克隆

        WeeklyLog weeklyLog = new WeeklyLog();
        weeklyLog.setName("张三");
        weeklyLog.setDate(String.valueOf(System.currentTimeMillis()));
        Attachment attachment = new Attachment();
        attachment.setName("附件xxx");
        weeklyLog.setAttachment(attachment);
        System.out.println(weeklyLog.toString());
        WeeklyLog weeklyLog2 = null;
        try {
            weeklyLog2 = weeklyLog.deepClone();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        System.out.println(weeklyLog2.toString());
        //比较周报
        System.out.println("周报是否相同？ " + (weeklyLog == weeklyLog2));
        //比较附件
        System.out.println("附件是否相同？ " + (weeklyLog.getAttachment() == weeklyLog2.getAttachment()));
    }
}
