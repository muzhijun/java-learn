package com.ljj.pattern.configfactory.builder.example4;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 11:06
 */
public class WorkBuilder {

    private HouseParam params;

    public WorkBuilder() {
        this.params = new HouseParam();
    }

    public WorkBuilder makeWindow(String window) {
        this.params.window = window;
        return this;
    }

    public WorkBuilder makeFloor(String floor) {
        this.params.floor = floor;
        return this;
    }

    public House build() {
        House house = new House();
        house.apply(this.params);
        return house;
    }

    class HouseParam {
        public String window;
        public String floor;
    }

}
