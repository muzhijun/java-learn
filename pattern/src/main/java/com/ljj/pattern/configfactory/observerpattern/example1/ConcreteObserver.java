package com.ljj.pattern.configfactory.observerpattern.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/23 21:17
 */
public class ConcreteObserver implements Observer {
    @Override
    public void update() {
        // 具体的相应代码
    }
}
