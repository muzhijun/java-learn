package com.ljj.pattern.configfactory.iteratorpattern.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 21:45
 */
public interface Aggregate {

    Iterator createIterator();
}
