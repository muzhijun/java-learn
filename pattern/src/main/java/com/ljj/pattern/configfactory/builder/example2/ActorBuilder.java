package com.ljj.pattern.configfactory.builder.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:26
 */
public abstract class ActorBuilder {

    protected Actor actor = new Actor();

    public abstract void buildType();

    public abstract void buildSex();

    public abstract void buildFace();

    public abstract void buildCostume();

    public abstract void buildHairstyle();

    public Actor getActor() {
        return this.actor;
    }
}
