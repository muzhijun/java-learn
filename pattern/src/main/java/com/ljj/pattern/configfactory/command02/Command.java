package com.ljj.pattern.configfactory.command02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:33
 */
public interface Command {

    /**
     * 执行
     */
    void execute();
}
