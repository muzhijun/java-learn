package com.ljj.pattern.configfactory.builder.example3;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 10:52
 */
public class WorkBuilder implements Builder {

    private House house = new House();

    @Override
    public void makeWindow() {
        this.house.setWindow("工人安装窗户");
    }

    @Override
    public void makeFloor() {
        this.house.setFloor("工人安装地板");
    }

    @Override
    public House getHouse() {
        return this.house;
    }
}
