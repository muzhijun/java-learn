package com.ljj.pattern.configfactory.chainpattern03;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:38
 */
public interface StudyPrepareFilter {

    void doFilter(PreparationList preparationList, FilterChain filterChain);
}
