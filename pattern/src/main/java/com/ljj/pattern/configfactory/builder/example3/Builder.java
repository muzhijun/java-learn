package com.ljj.pattern.configfactory.builder.example3;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 10:48
 */
public interface Builder {

    /**
     * 建造窗户
     */
    void makeWindow();

    /**
     * 建造地板
     */
    void makeFloor();

    /**
     * 得到房子
     *
     * @return {@link House}
     */
    House getHouse();
}
