package com.ljj.pattern.configfactory.prototype;

import java.io.*;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/5 13:58
 */
public class WeeklyLog implements Serializable {

    private String name;
    private String date;
    private String content;
    private Attachment attachment;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Attachment getAttachment() {
        return this.attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return "WeeklyLog{" +
                "name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", content='" + content + '\'' +
                ", attachment=" + attachment +
                '}';
    }

    // 浅克隆
    ////@Override
    //public WeeklyLog clone() {
    //    Object obj = null;
    //    try {
    //        obj = super.clone();
    //        return (WeeklyLog) obj;
    //    } catch (CloneNotSupportedException e) {
    //        System.out.println("不支持复制");
    //        return null;
    //    }
    //}

    public WeeklyLog deepClone() throws IOException, ClassNotFoundException {
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bao);
        oos.writeObject(this);

        ByteArrayInputStream bai = new ByteArrayInputStream(bao.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bai);
        return (WeeklyLog) ois.readObject();
    }
}
