package com.ljj.pattern.configfactory.prototype;

import java.io.Serializable;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/5 14:17
 */
public class Attachment implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "name='" + name + '\'' +
                '}';
    }
}
