package com.ljj.pattern.configfactory.interpret;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:53
 */
public class InterpreterPatternDemo {

    public static void main(String[] args) {
        Context bus = new Context();
        bus.freeRide("韶关的老人");
        bus.freeRide("韶关的年轻人");
        bus.freeRide("广州的妇女");
        bus.freeRide("广州的儿童");
        bus.freeRide("山东的儿童");
        
    }
}
