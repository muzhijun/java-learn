package com.ljj.pattern.configfactory.chainpattern02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:04
 */
public class EatBreakfastPrepareFilter extends AbstractPrepareFilter {

    public EatBreakfastPrepareFilter(AbstractPrepareFilter nextFilter) {
        super(nextFilter);
    }

    @Override
    public void prepare(PreparedList preparedList) {
        if (!preparedList.isEatBreakfast()) {
            System.out.println("吃早饭");
        }
    }
}
