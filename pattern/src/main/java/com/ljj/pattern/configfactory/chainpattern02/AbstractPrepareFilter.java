package com.ljj.pattern.configfactory.chainpattern02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 10:59
 */
public abstract class AbstractPrepareFilter {

    public AbstractPrepareFilter nextFilter;

    public AbstractPrepareFilter(AbstractPrepareFilter nextFilter) {
        this.nextFilter = nextFilter;
    }

    public abstract void prepare(PreparedList preparedList);

    public void doFilter(PreparedList preparedList, Study study) {
        prepare(preparedList);

        if (nextFilter != null) {
            nextFilter.doFilter(preparedList, study);
        } else {
            study.study();
        }
    }
}
