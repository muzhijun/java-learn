package com.ljj.pattern.configfactory.builder.example3;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 10:55
 */
public class Client {

    public static void main(String[] args) {
        WorkBuilder workBuilder = new WorkBuilder();
        Director director = new Director();
        House house = director.design(workBuilder);
        System.out.println(house.toString());
    }
}
