package com.ljj.pattern.configfactory.command02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:36
 */
public class Client {

    public static void main(String[] args) {

        // 创建接收者
        AudioPlayer audioPlayer = new AudioPlayer();
        // 创建命令对象，设定它的接收者
        Command playCommand = new PlayCommand(audioPlayer);
        Command rewindCommand = new RewindCommand(audioPlayer);
        Command stopCommand = new StopCommand(audioPlayer);
        // 创建请求者，把命令对象设置进去
        Keyboard keyPad = new Keyboard();
        keyPad.setPlayCommand(playCommand);
        keyPad.setRewindCommand(rewindCommand);
        keyPad.setStopCommand(stopCommand);
        // 执行方法
        keyPad.play();
        keyPad.rewind();
        keyPad.stop();
    }
}
