package com.ljj.pattern.configfactory.builder.example3;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 10:48
 */
public class House {

    private String window;
    private String floor;

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "House{" +
                "window='" + window + '\'' +
                ", floor='" + floor + '\'' +
                '}';
    }
}
