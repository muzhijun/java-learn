package com.ljj.pattern.configfactory.builder.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:10
 */
public class ConcreteBuilder extends Builder {
    @Override
    public void buildPartA() {
        this.product.setPartA("安装显示器");
        System.out.println("安装显示器");
    }

    @Override
    public void buildPartB() {
        this.product.setPartB("安装CPU");
        System.out.println("安装CPU");
    }

    @Override
    public void buildPartC() {
        this.product.setPartC("安装GPU");
        System.out.println("安装GPU");
    }
}
