package com.ljj.pattern.configfactory.chainPattern;

/**
 * @author lijunjun
 * @className Congress
 * @description Congress
 * @date 2022/7/18 14:54
 */
public class Congress extends Approver {
    public Congress(String name) {
        super(name);
    }

    @Override
    public void processRequest(PurchaseRequest request) {
        System.out.println("召开董事会" + this.name + "审批采购单：" + request.getNumber() + "，金额：" + request.getAmount() + "元，采购目的：" + request.getPurpose() + "。");  //处理请求
    }
}
