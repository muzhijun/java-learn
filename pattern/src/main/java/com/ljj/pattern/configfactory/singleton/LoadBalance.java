package com.ljj.pattern.configfactory.singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/8 18:52
 */
public class LoadBalance {

    private LoadBalance() {
        serverList = new ArrayList<>();
    }

    private List<String> serverList;

    private static LoadBalance loadBalance = null;

    public static LoadBalance getInstance() {
        if (loadBalance == null) {
            loadBalance = new LoadBalance();
        }
        return loadBalance;
    }

    public void addServer(String server) {
        this.serverList.add(server);
    }

    public void removeServer(String server) {
        this.serverList.remove(server);
    }

    public String getServer() {
        Random random = new Random();
        int num = random.nextInt(this.serverList.size());
        return this.serverList.get(num);
    }

}
