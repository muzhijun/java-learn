package com.ljj.pattern.configfactory.builder.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:44
 */
public class AngleBuild extends ActBuilder2 {
    @Override
    public void buildType() {
        this.actor.setType("天使");
    }

    @Override
    public void buildSex() {
        this.actor.setSex("女");
    }

    @Override
    public void buildFace() {
        this.actor.setHairstyle("漂亮");
    }

    @Override
    public void buildCostume() {
        this.actor.setCostume("白裙");
    }

    @Override
    public void buildHairstyle() {
        this.actor.setHairstyle("披肩长发");
    }
}
