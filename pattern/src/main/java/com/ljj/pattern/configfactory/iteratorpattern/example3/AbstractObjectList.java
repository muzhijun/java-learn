package com.ljj.pattern.configfactory.iteratorpattern.example3;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 21:55
 */
public abstract class AbstractObjectList {

    protected List<Object> objects = new ArrayList<Object>();

    public AbstractObjectList(List<Object> objects) {
        this.objects = objects;
    }

    public void addObject(Object object) {
        this.objects.add(object);
    }

    public void removeObject(Object object) {
        this.objects.remove(object);
    }

    public List getObjects() {
        return this.objects;
    }

    // 创建迭代器方法
    public AbstractIterator createIterator() {
        return null;
    }
}
