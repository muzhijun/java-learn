package com.ljj.pattern.configfactory.prototype.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/5 15:49
 */
public class SRS implements OfficialDocument {
    @Override
    public OfficialDocument clone() {

        OfficialDocument od = null;
        try {
            od = (OfficialDocument) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
        return od;
    }

    @Override
    public void display() {
        System.out.println("需求软件分析");
    }
}
