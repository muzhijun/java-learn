package com.ljj.pattern.configfactory.iteratorpattern.example3;

import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 22:01
 */
public class ProductIterator implements AbstractIterator {

    protected AbstractObjectList objectList;
    private List products;
    private int cursor;// 正向遍历游标
    private int cursor1;// 逆向遍历游标

    public ProductIterator(ProductList productList) {
        this.objectList = productList;
        this.products = productList.getObjects();
        this.cursor = 0;
        this.cursor1 = this.products.size() - 1;
    }

    @Override
    public void next() {
        if (this.cursor < this.products.size()) {
            this.cursor++;
        }
    }

    @Override
    public boolean isLast() {
        return this.cursor == this.products.size();
    }

    @Override
    public void previous() {
        if (this.cursor1 > -1) {
            this.cursor1--;
        }
    }

    @Override
    public boolean isFirst() {
        return this.cursor1 == -1;
    }

    @Override
    public Object getNextItem() {
        return this.products.get(cursor);
    }

    @Override
    public Object getPreviousItem() {
        return this.products.get(this.cursor1);
    }
}
