package com.ljj.pattern.configfactory.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author lijunjun
 * @title 描述
 * @date 2021/12/26 20:07
 */
public class CoffeeFactory {

    private static Map<String, Coffee> map = new HashMap<>();

    static {
        Properties props = new Properties();
        InputStream is = CoffeeFactory.class.getClassLoader().getResourceAsStream("bean.properties");
        try {
            // 从properties中加载内容
            props.load(is);
            Set<Object> keys = props.keySet();
            for (Object key : keys) {
                String className = (String) props.get(key);
                Class clazz = Class.forName(className);
                Coffee coffee = (Coffee) clazz.newInstance();
                map.put((String) key, coffee);
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据名称获取咖啡
     *
     * @param name
     * @return
     */
    public static Coffee createCoffee(String name) {
        return map.get(name);
    }
}
