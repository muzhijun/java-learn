package com.ljj.pattern.configfactory.command02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:33
 */
public class PlayCommand implements Command {

    private AudioPlayer audioPlayer;

    public PlayCommand(AudioPlayer audioPlayer) {
        this.audioPlayer = audioPlayer;
    }

    @Override
    public void execute() {
        audioPlayer.play();
    }
}
