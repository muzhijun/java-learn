package com.ljj.pattern.configfactory.builder.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/2 20:30
 */
public class ActorController {
    public Actor construct(ActorBuilder actorBuilder) {
        actorBuilder.buildType();
        actorBuilder.buildSex();
        actorBuilder.buildFace();
        actorBuilder.buildCostume();
        actorBuilder.buildHairstyle();
        return actorBuilder.actor;
    }

}
