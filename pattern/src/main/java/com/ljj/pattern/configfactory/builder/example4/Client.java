package com.ljj.pattern.configfactory.builder.example4;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/2/2 11:15
 */
public class Client {

    public static void main(String[] args) {
        WorkBuilder workBuilder = new WorkBuilder();
        House house = workBuilder.makeWindow("建造窗户").makeFloor("建造地板").build();
        System.out.println(house.toString());
    }
}
