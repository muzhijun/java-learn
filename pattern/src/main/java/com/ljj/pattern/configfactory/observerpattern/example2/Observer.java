package com.ljj.pattern.configfactory.observerpattern.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/23 21:24
 */
//抽象观察类
public interface Observer {

    public String getName();

    public void setName(String name);

    public void help(); //声明支援盟友方法

    public void beAttacked(AllyControlCenter acc); //声明遭受攻击方法
}
