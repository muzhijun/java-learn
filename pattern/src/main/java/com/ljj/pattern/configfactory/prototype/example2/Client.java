package com.ljj.pattern.configfactory.prototype.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/5 15:51
 */
public class Client {

    public static void main(String[] args) {

        PrototypeManager pm = PrototypeManager.getPrototypeManager();
        pm.getOfficalDocument("far").display();
        pm.getOfficalDocument("srs").display();


    }
}
