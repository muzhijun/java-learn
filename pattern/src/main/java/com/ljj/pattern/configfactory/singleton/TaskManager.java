package com.ljj.pattern.configfactory.singleton;

import java.util.Objects;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/8 18:35
 */
public class TaskManager {

    // 私有化构造方法，禁止使用外部使用new构造实例
    private TaskManager() {
    }

    private static TaskManager tm = null;

    public static TaskManager getInstance() {
        if (Objects.isNull(tm)) {
            tm = new TaskManager();
        }
        return tm;
    }
}
