package com.ljj.pattern.configfactory.command;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:24
 */
public class Client {

    public static void main(String[] args) {
        // 创建接收者
        Receiver receiver = new Receiver();
        // 创建命令对象，设定它的接收者
        ICommand command = new ConcreteCommand(receiver);
        // 创建请求者，把命令对象设置进去
        Invoker invoker = new Invoker(command);
        // 执行方法
        invoker.action();
    }
}
