package com.ljj.pattern.configfactory.chainpattern02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:00
 */
public class WashFacePrepareFilter extends AbstractPrepareFilter {

    public WashFacePrepareFilter(AbstractPrepareFilter nextFilter) {
        super(nextFilter);
    }

    @Override
    public void prepare(PreparedList preparedList) {
        if (!preparedList.isWashFace()) {
            System.out.println("洗脸");
        }
    }
}
