package com.ljj.pattern.configfactory.singleton;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/8 18:39
 */
public class Client {

    public static void main(String[] args) {

        //TaskManager tm = TaskManager.getInstance();
        //for (int i = 0; i < 1000000; i++) {
        //    new Thread(() -> {
        //        if (!tm.equals(TaskManager.getInstance())) {
        //            System.out.println("存在不一致");
        //        }
        //    }).start();
        //    System.out.println("子线程执行结束");
        //}
        //System.out.println("主线程结束");

        LoadBalance lb1, lb2, lb3;
        lb1 = LoadBalance.getInstance();
        lb2 = LoadBalance.getInstance();
        lb3 = LoadBalance.getInstance();

       
        if (lb1.equals(lb2) && lb2.equals(lb2) && lb3.equals(lb1)) {
            System.out.println("单例模式成功");
        }

        lb1.addServer("服务器1");
        lb1.addServer("服务器2");
        lb1.addServer("服务器3");

        for (int i = 0; i < 15; i++) {
            System.out.println(lb1.getServer());
        }
    }
}
