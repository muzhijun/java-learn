package com.ljj.pattern.configfactory.prototype.example2;

import java.util.Hashtable;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/8/5 15:51
 */
public class PrototypeManager {

    private Hashtable ht = new Hashtable();

    private static PrototypeManager prototypeManager = new PrototypeManager();

    private PrototypeManager() {
        this.ht.put("far", new FAR());
        this.ht.put("srs", new SRS());
    }

    public void add(String key, OfficialDocument document) {
        this.ht.put(key, document);
    }

    public OfficialDocument getOfficalDocument(String key) {
        return (OfficialDocument) this.ht.get(key);
    }

    public static PrototypeManager getPrototypeManager() {
        return prototypeManager;
    }
}
