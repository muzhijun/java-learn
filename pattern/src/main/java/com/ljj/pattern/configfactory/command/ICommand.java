package com.ljj.pattern.configfactory.command;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:17
 */
public interface ICommand {

    void execute();
}
