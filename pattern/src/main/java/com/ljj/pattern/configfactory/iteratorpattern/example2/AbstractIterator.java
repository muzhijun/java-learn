package com.ljj.pattern.configfactory.iteratorpattern.example2;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 21:58
 */
public interface AbstractIterator {

    void next(); //移至下一个元素

    boolean isLast(); //判断是否为最后一个元素

    void previous(); //移至上一个元素

    boolean isFirst(); //判断是否为第一个元素

    Object getNextItem(); //获取下一个元素

    Object getPreviousItem(); //获取上一个元素

}
