package com.ljj.pattern.configfactory.chainpattern03;

import com.ljj.pattern.configfactory.chainpattern02.Study;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:39
 */
public class FilterChain implements StudyPrepareFilter {

    // 定义下面两个属性，用于记录当前执行到哪个过滤器了
    private int pos = 0;

    private PreparationList preparationList;

    private List<StudyPrepareFilter> filters = new ArrayList<>();

    private Study study;

    public FilterChain(PreparationList preparationList, Study study) {
        this.preparationList = preparationList;
        this.study = study;
    }

    @Override
    public void doFilter(PreparationList preparationList, FilterChain filterChain) {
        if (pos < filters.size()) {
            StudyPrepareFilter filter = filters.get(pos);
            pos++;
            filter.doFilter(preparationList, filterChain);
        } else {
            study.study();
        }
    }

    public void addFilter(StudyPrepareFilter filter) {
        filters.add(filter);
    }
}
