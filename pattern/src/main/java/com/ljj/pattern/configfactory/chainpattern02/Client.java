package com.ljj.pattern.configfactory.chainpattern02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:06
 */
public class Client {

    public static void main(String[] args) {

        // 此种方式为普通版责任链模式，如果需要添加新的过滤器，需要修改原有的代码，不符合开闭原则
        PreparedList preparedList = new PreparedList();
        preparedList.setEatBreakfast(false);
        preparedList.setWashFace(true);
        preparedList.setWashHair(false);

        EatBreakfastPrepareFilter eatBreakfastPrepareFilter = new EatBreakfastPrepareFilter(null);
        WashHairPrepareFilter washHairPrepareFilter = new WashHairPrepareFilter(eatBreakfastPrepareFilter);
        WashFacePrepareFilter washFacePrepareFilter = new WashFacePrepareFilter(washHairPrepareFilter);
        // 添加新的链路，需要在此位置建立责任链的关系

        Study study = new Study();
        washFacePrepareFilter.doFilter(preparedList, study);
    }
}
