package com.ljj.pattern.configfactory.interpret;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:49
 */
public interface Expression {

    /**
     * 解释
     *
     * @param context 上下文
     * @return boolean
     */
    boolean interpret(String context);
}
