package com.ljj.pattern.configfactory.iteratorpattern.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 21:46
 */
public class ConcreteAggregate implements Aggregate {
    @Override
    public Iterator createIterator() {
        return new ConcreteIterator(this);
    }
}
