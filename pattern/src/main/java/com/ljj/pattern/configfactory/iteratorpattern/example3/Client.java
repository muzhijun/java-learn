package com.ljj.pattern.configfactory.iteratorpattern.example3;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 22:11
 */
public class Client {

    public static void main(String[] args) {

        List products = new ArrayList();
        products.add("倚天剑");
        products.add("屠龙刀");
        products.add("断肠草");
        products.add("葵花宝典");
        products.add("四十二章经");

        ProductList productList = new ProductList(products);
        AbstractIterator productListIterator = productList.createIterator();
        while (!productListIterator.isLast()) {
            System.out.println(productListIterator.getNextItem());
            productListIterator.next();
        }
        while (!productListIterator.isFirst()) {
            System.out.println(productListIterator.getPreviousItem());
            productListIterator.previous();
        }


        ListIterator<Object> listIterator = products.listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }
        while (listIterator.hasPrevious()) {
            System.out.println(listIterator.previous());
        }


    }
}
