package com.ljj.pattern.configfactory.chainpattern02;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:15
 */
public class PreparedList {

    private boolean washFace;

    private boolean washHair;

    private boolean eatBreakfast;

    public boolean isWashFace() {
        return washFace;
    }

    public void setWashFace(boolean washFace) {
        this.washFace = washFace;
    }

    public boolean isWashHair() {
        return washHair;
    }

    public void setWashHair(boolean washHair) {
        this.washHair = washHair;
    }

    public boolean isEatBreakfast() {
        return eatBreakfast;
    }

    public void setEatBreakfast(boolean eatBreakfast) {
        this.eatBreakfast = eatBreakfast;
    }
}
