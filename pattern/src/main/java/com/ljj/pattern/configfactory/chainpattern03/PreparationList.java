package com.ljj.pattern.configfactory.chainpattern03;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 11:38
 */
public class PreparationList {

    private boolean washFace;
    private boolean washHair;
    private boolean haveBreakfast;

    public boolean isWashFace() {
        return washFace;
    }

    public void setWashFace(boolean washFace) {
        this.washFace = washFace;
    }

    public boolean isWashHair() {
        return washHair;
    }

    public void setWashHair(boolean washHair) {
        this.washHair = washHair;
    }

    public boolean isHaveBreakfast() {
        return haveBreakfast;
    }

    public void setHaveBreakfast(boolean haveBreakfast) {
        this.haveBreakfast = haveBreakfast;
    }
}
