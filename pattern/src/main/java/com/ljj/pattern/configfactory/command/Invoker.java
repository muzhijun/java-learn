package com.ljj.pattern.configfactory.command;

/**
 * @author lijunjun
 * @title 描述
 * @date 2023/10/10 14:23
 */
public class Invoker {

    private ICommand command;

    public Invoker(ICommand command) {
        this.command = command;
    }

    public void action() {
        command.execute();
    }
}
