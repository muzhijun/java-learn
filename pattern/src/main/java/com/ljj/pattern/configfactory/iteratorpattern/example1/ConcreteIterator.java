package com.ljj.pattern.configfactory.iteratorpattern.example1;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/7/19 21:43
 */
public class ConcreteIterator implements Iterator {

    private ConcreteAggregate obj;

    private int cursor;

    public ConcreteIterator(ConcreteAggregate obj) {
        this.obj = obj;
    }

    @Override
    public void first() {

    }

    @Override
    public void next() {

    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public void last() {
    }
}
