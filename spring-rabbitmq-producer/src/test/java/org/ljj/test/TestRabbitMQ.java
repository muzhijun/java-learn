package org.ljj.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-rabbitmq-producer.xml")
public class TestRabbitMQ {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送helloWorld消息
     */
    @Test
    public void testSendHelloWorld(){
        rabbitTemplate.convertAndSend("spring_queue", "hello world~~~~");
    }

    /**
     * 测试fanout
     */
    @Test
    public void testFanout(){
        rabbitTemplate.convertAndSend("spring_fanout_exchange", "", "hello spring_fanout_exchange~~~~");
    }

    /**
     * 测试topic
     */
    @Test
    public void testTopic(){
        rabbitTemplate.convertAndSend("spring_topic_exchange", "heima.hehe", "hello spring_topic_exchange~~~~");
    }
}
