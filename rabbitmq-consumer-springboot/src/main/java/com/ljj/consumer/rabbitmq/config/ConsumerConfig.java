package com.ljj.consumer.rabbitmq.config;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class ConsumerConfig {

//    @RabbitListener(queues = "boot_queue")
//    public void listenerConsumer(Message message){
//        System.out.println("消费了消息：" + new String(message.getBody()));
//    }

    /*
    @RabbitListener 注解用来监听消息队列中的消息
        queues 参数用来识别具体接收什么队列
        String message 记录消息的body内容 如果参数是Message message，则message记录更多的参数
     */
    @RabbitListener(queues = "boot_queue")
    public void listenerConsumer(String message){
        System.out.println("消费了消息：" + message);
        //真正的处理逻辑的地方
    }

}
