package com.ljj.producer;

import com.ljj.producer.rabbit.config.RabbitMQConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TextProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testProducer(){
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_NAME, "ljj.haha", "hi rabbitmq-producer-springboot~~~");
    }
}
