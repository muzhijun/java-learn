package com.ljj.producer.rabbit.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String EXCHANGE_NAME = "boot_exchange";
    public static final String QUEUE_NAME = "boot_queue";

    /**
     * 注入交换机
     * @return
     */
    @Bean("bootExchange")
    public Exchange bootExchange(){
        return ExchangeBuilder.topicExchange(EXCHANGE_NAME).durable(true).build();
    }

    /**
     * 注入队列
     * @return
     */
    @Bean("bootQueue")
    public Queue bootQueue(){
        return QueueBuilder.durable(QUEUE_NAME).build();
    }

    /**
     * 将queue与exchange完成绑定
     * @param queue
     * @param exchange
     * @return
     */
    @Bean
    public Binding bindQueueExchange(Queue queue, Exchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("ljj.#").noargs();
    }
}
