package com.ljj.nio;

import java.nio.ByteBuffer;

public class TestTypeBufferRead {

    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put(new byte[]{'a', 'b', 'c', 'd'});
        buffer.flip();// 切换为读模式

        // rewind position 恢复到起点重新读
        /*System.out.println((char)buffer.get());
        System.out.println((char)buffer.get());
        buffer.rewind();
        System.out.println((char)buffer.get());*/

        // mark & reset
        /*System.out.println((char) buffer.get());
        System.out.println((char) buffer.get());
        buffer.mark();// 标记当前位置
        System.out.println((char) buffer.get());
        System.out.println((char) buffer.get());
        buffer.reset();// 重制 position，恢复到标记位置
        System.out.println((char) buffer.get());*/

        // get(i) 表示从指定位置开始读取，不改变 position 的值
        System.out.println((char) buffer.get(2));
    }
}
