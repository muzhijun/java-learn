package com.ljj.nio;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class TestByteBufferString {

    public static void main(String[] args) {
        // 字符串转TybeBuffer
        ByteBuffer buffer1 = ByteBuffer.allocate(10);
        buffer1.put("hello".getBytes());
        buffer1.flip();// 切换到读模式
        System.out.println(buffer1.get());

        // Charset转化
        ByteBuffer buffer2 = StandardCharsets.UTF_8.encode("hello");
//        System.out.println(buffer2.get());

        String str2 = StandardCharsets.UTF_8.decode(buffer2).toString();
        System.out.println(str2);

        // wrap转化
        ByteBuffer buffer3 = ByteBuffer.wrap("hello".getBytes());
        System.out.println(buffer3.get());

    }
}
