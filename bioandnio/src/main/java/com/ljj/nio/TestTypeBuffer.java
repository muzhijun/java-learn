package com.ljj.nio;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@Slf4j
public class TestTypeBuffer {

    public static void main(String[] args) {
        try (FileChannel fileChannel = new FileInputStream("./data.txt").getChannel()){
            ByteBuffer byteBuffer = ByteBuffer.allocate(10);// 初始化bytebuffer并分配空间
            while (true){
                // 从channel中读取，写入到bytebuffer中
                int len = fileChannel.read(byteBuffer);
                log.debug("读取到的字节{}", len);
                if (len == -1){// 如果读取结束
                    break;
                }
                byteBuffer.flip();// 切换到读模式
                while (byteBuffer.hasRemaining()){
                    byte b = byteBuffer.get();
                    char info = (char)b;
                    log.debug("实际读取到的字节" + info);
                }
                byteBuffer.clear();// 切换为写模式
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
