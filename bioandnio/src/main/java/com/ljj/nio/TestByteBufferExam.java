package com.ljj.nio;


import java.nio.ByteBuffer;

public class TestByteBufferExam {

    public static void main(String[] args) {

        ByteBuffer source = ByteBuffer.allocate(32);
        source.put("hello world\nI am zhangsan\nH".getBytes());
        split(source);
        source.put("ow are you?\n".getBytes());
        split(source);
    }

    private static void split(ByteBuffer source){
        source.flip();// 切换读模式
        for (int i = 0; i < source.limit(); i++) {
            if (source.get(i) == '\n'){// 查找到 \n 符号，则开始进行截取
                // 获取每个分割的长度
                int len = i - source.position() + 1;
                ByteBuffer target = ByteBuffer.allocate(len);
                //从source中循环读取放入到target中
                for (int j = 0; j < len; j++) {
                    target.put(source.get());
                }

                target.flip();//切换到读模式
                System.out.println(new String(target.array()));
            }
        }
        source.compact();// 切换写模式（保留未读部分）
    }
}
