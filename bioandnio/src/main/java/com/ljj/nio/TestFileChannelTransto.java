package com.ljj.nio;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class TestFileChannelTransto {

    public static void main(String[] args) {

        try (
                FileChannel from = new FileInputStream("data.txt").getChannel();
                FileChannel to = new FileOutputStream("to.txt").getChannel()
        ) {
            //会使用操作系统的零拷贝，效率高
            //当前方法有限制，一次最多拷贝2g
//            from.transferTo(0, from.size(), to);

            //进行改造 改造后的方式没有大小的限制
            long size = from.size();
            for (long left = size;  left > 0;){
                left -= from.transferTo((size-left), left, to);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
