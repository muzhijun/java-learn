package com.ljj.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestFileCopy {

    public static void main(String[] args) throws IOException {
        // 利用Files.walk进行文件复制
        String sourcePath  = "/Users/leadu/Downloads/chrome-download/ide-eval-resetter";
        String targetPath  = "/Users/leadu/Downloads/chrome-download/ide-eval-resetterxxx";
        Files.walk(Paths.get(sourcePath)).forEach(path -> {
            try {
                String targetName = path.toString().replace(sourcePath, targetPath);
                if (Files.isDirectory(path)) {// 如果是文件夹
                    // 生成新的文件夹目录
                    Files.createDirectory(Paths.get(targetName));
                }
                // 如果是文件
                else if (Files.isRegularFile(path)){
                    Files.copy(path, Paths.get(targetName));
                }
            } catch (IOException e){

            }
        });
    }
}
