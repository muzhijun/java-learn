package com.ljj.nio;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class TestScatteringRead {

    public static void main(String[] args) {

        // 分散读取
        try (FileChannel fileChannel = new RandomAccessFile("words.txt", "r").getChannel()){
            ByteBuffer b1 = ByteBuffer.allocate(2);
            ByteBuffer b2 = ByteBuffer.allocate(2);
            ByteBuffer b3 = ByteBuffer.allocate(3);
            //自动根据byteBuffer的capacity进行填充
            fileChannel.read(new ByteBuffer[]{b1, b2, b3});
            b1.flip();
            b2.flip();
            b3.flip();
            System.out.println(new String(b1.array()));
            System.out.println(new String(b2.array()));
            System.out.println(new String(b3.array()));
        } catch (IOException e){

        }

    }
}
