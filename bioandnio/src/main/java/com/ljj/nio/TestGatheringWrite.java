package com.ljj.nio;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

public class TestGatheringWrite {

    public static void main(String[] args) {

        // 分散写入
        ByteBuffer b1 = StandardCharsets.UTF_8.encode("abc");
        ByteBuffer b2 = StandardCharsets.UTF_8.encode("123");
        ByteBuffer b3 = StandardCharsets.UTF_8.encode("efg");
        try (FileChannel fileChannel = new RandomAccessFile("words2.txt", "rw").getChannel()){
            fileChannel.write(new ByteBuffer[]{b1,b2,b3});
        } catch (IOException e){

        }
    }
}
