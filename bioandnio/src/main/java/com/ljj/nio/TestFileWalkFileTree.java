package com.ljj.nio;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;

public class TestFileWalkFileTree {

    public static void main(String[] args) throws IOException {

//        m1();
        AtomicInteger classCount =  new AtomicInteger(0);
        Files.walkFileTree(Paths.get("/Users/leadu/IdeaProjects/java-learn"), new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.toString().endsWith(".class")) {
                    classCount.incrementAndGet();
                }
                return super.visitFile(file, attrs);
            }
        });
        System.out.println("class文件的数量："  + classCount);

    }

    private static void m1() throws IOException {
        AtomicInteger dirCount =  new AtomicInteger(0);
        AtomicInteger fileCount =  new AtomicInteger(0);

        Files.walkFileTree(Paths.get("/Users/leadu/IdeaProjects/java-learn"), new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                System.out.println("=====>" + dir.toString());
                dirCount.incrementAndGet();
                return super.preVisitDirectory(dir, attrs);
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                System.out.println(file.toString());
                fileCount.incrementAndGet();
                return super.visitFile(file, attrs);
            }
        });

        System.out.println("dirCount==>" + dirCount);
        System.out.println("fileCount==>" + fileCount);
    }

    public static void deleteDicAndFiles() throws IOException {

        /*
        删除目录下所有文件和目录，需要保证文件先被删除，然后再删除目录
            1 实现visitFile，删除文件
            2 文件被删除后，实现postVisitDirectory删除剩下的所有空目录
         */

        Files.walkFileTree(Paths.get("/Document/docker"), new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                //删除所有文件
                Files.delete(file);
                return super.visitFile(file, attrs);
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                // 删除所有目录
                Files.delete(dir);
                return super.postVisitDirectory(dir, exc);
            }
        });
    }
}
