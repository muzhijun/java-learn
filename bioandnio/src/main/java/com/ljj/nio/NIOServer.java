package com.ljj.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {

    public static void main(String[] args) throws IOException {
        //创建一个服务端channel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //创建一个selector
        Selector selector = Selector.open();
        //绑定一个端口，在服务端监听
        serverSocketChannel.socket().bind(new InetSocketAddress(6666));
        //设置为非阻塞
        serverSocketChannel.configureBlocking(false);
        //serverSocketChannel注册到selector,并设置关心事件
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        //循环等待客户连接
        while (true){//一次循环解决一个事件集合里的全部事件，下次循环会再继续监听
            if (selector.select(1000) == 0){//无事发生，继续等待
                System.out.println("服务器无事发生，继续等待1s");
                continue;
            }
            //如果有事发生，获取发生事件的集合
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            if (iterator.hasNext()){
                SelectionKey selectionKey = iterator.next();
                //查看发生的事件并作相应的处理
                if (selectionKey.isAcceptable()){
                    //为这个客户端生成一个socketchannel
                    SocketChannel socketChannel = serverSocketChannel.accept();
                    socketChannel.configureBlocking(false);
                    System.out.println("客户端连接成功，" + socketChannel.hashCode());
                    //将当前的channel注册到selector上
                    socketChannel.register(selector, SelectionKey.OP_READ, ByteBuffer.allocate(1024));
                }
                if (selectionKey.isReadable()){
                    //发生了读事件，通过key反向得到channel和buffer
                    SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
                    //获取与该channel关联的buffer，在和连接时就已经绑定了
                    ByteBuffer byteBuffer = (ByteBuffer) selectionKey.attachment();
                    socketChannel.read(byteBuffer);
                    byteBuffer.flip();
                    System.out.println("读取到来自客户端的消息：" + new String(byteBuffer.array()));
                    byteBuffer.clear();
                }
            }
            //及时将当前的SelectionKey,防止操作
            iterator.remove();
        }
    }
}
