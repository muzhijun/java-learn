package com.ljj.simple;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class MultiThreadServer {

    public static void main(String[] args) throws IOException {
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.bind(new InetSocketAddress(8080));
        ssc.configureBlocking(false);

        Selector boss = Selector.open();
        ssc.register(boss, SelectionKey.OP_ACCEPT);

        Worker[] workers = new Worker[2];
        for (int i = 0; i < 2; i++) {
            workers[i] = new Worker("worker-" + i);
        }
        AtomicInteger index = new AtomicInteger(0);

        while (true){
            boss.select();

            Iterator<SelectionKey> iterators = boss.selectedKeys().iterator();
            while (iterators.hasNext()){
                SelectionKey key = iterators.next();
                iterators.remove();

                if (key.isAcceptable()) {
                    SocketChannel channel = ssc.accept();
                    channel.configureBlocking(false);

                    log.debug("connected... {}", channel.getRemoteAddress());
                    log.debug("before register... {}", channel.getRemoteAddress());
                    //轮询
                    workers[index.getAndIncrement() % workers.length].register(channel);

                    //利用work进行注册
                    log.debug("after register... {}", channel.getRemoteAddress());
                }
            }
        }
    }

    static class Worker implements Runnable{

        private Thread thread;
        private Selector selector;
        private String name;
        private boolean start;
        private ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>();

        public Worker(String name) {
            this.name = name;
        }

        //初始化方法
         public void register(SocketChannel channel) throws IOException {
            if (!start){
                selector = Selector.open();
                //开启新的线程
                thread = new Thread(this, name);
                thread.start();
                start = true;
            }
            queue.add(()->{
                try {
                    channel.register(selector, SelectionKey.OP_READ, null);
                } catch (ClosedChannelException e) {
                    e.printStackTrace();
                }
            });
            //唤醒
            selector.wakeup();
        }

        @Override
        public void run() {
            while (true){
                try {
                    selector.select();
                    Runnable task = queue.poll();
                    if (task != null){
                        task.run();
                    }

                    Iterator<SelectionKey> iterators = selector.selectedKeys().iterator();
                    while (iterators.hasNext()){
                        SelectionKey key = iterators.next();
                        iterators.remove();

                        if (key.isReadable()){
                            ByteBuffer buffer = ByteBuffer.allocate(16);
                            SocketChannel channel = (SocketChannel) key.channel();

                            int read = channel.read(buffer);
                            log.debug("read size :{}", read);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
