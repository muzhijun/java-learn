package com.ljj.simple;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.Iterator;

@Slf4j
public class WriteServer {

    public static void main(String[] args) throws IOException {
        ServerSocketChannel ssc = ServerSocketChannel.open();
        //设置非阻塞
        ssc.configureBlocking(false);
        Selector selector = Selector.open();
        //注册到selector上并关注连接事件
        ssc.register(selector, SelectionKey.OP_ACCEPT);

        ssc.bind(new InetSocketAddress(8080));

        while (true){
            //监听，如果没有事件，会阻塞在这里
            selector.select();

            Iterator<SelectionKey> iterators = selector.selectedKeys().iterator();
            while (iterators.hasNext()){
                SelectionKey key = iterators.next();
                //及时移除当前的selectionkey
                iterators.remove();
                //如果是连接事件
                if (key.isAcceptable()){
                    //拿到连接的socketchannel，并注册到selector上
                    SocketChannel channel = ssc.accept();
                    channel.configureBlocking(false);
                    SelectionKey scKey = channel.register(selector, 0, null);
                    scKey.interestOps(SelectionKey.OP_READ);

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < 300000; i++) {
                        sb.append("a");
                    }
                    ByteBuffer buffer = Charset.defaultCharset().encode(sb.toString());
                    //返回值write代表每次写入的数据量
                    int write = channel.write(buffer);
                    log.debug("write size :{}", write);

                    //如果buffer中还有数据没有写完
                    if (buffer.hasRemaining()){
                        //对当前的selectionkey中新增关注一个可写事件
                        scKey.interestOps(scKey.interestOps() + SelectionKey.OP_WRITE);
                        //将剩余没写完的buffer放入到selectionkey中
                        scKey.attach(buffer);
                    }
                } else if (key.isWritable()){//如果是可写事件
                    ByteBuffer buffer = (ByteBuffer) key.attachment();
                    SocketChannel channel = (SocketChannel) key.channel();
                    int write = channel.write(buffer);
                    log.debug("write size :{}", write);
                    if (!buffer.hasRemaining()){//如果buffer中没有数据需要继续写入，则清空selectionkey中对应的buffer
                        key.attach(null);
                        key.interestOps(key.interestOps() - SelectionKey.OP_WRITE);
                    }
                }
            }
        }
    }
}
