package com.ljj.simple;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

@Slf4j
public class NServer {

    public static void main(String[] args) throws IOException {
        ServerSocketChannel serverSocketChannel =  ServerSocketChannel.open();
        //绑定端口
        serverSocketChannel.bind(new InetSocketAddress(8080));
        //设置非阻塞
        serverSocketChannel.configureBlocking(false);
        Selector selector  =  Selector.open();
        //将serverSocketChannel注册到Selector上，并设置监听的事件类型
        SelectionKey register = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        log.debug("注册的selectionkey是{}", register);

        while (true){
            //select方法在没有连接 的时候会阻塞在这里
            log.debug("waiting connect");
            selector.select();
            Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
            while (keyIterator.hasNext()){
                log.debug("size:{}", selector.selectedKeys().size());
                SelectionKey key = keyIterator.next();
                //移除当前迭代的对象，防止重复读取
                keyIterator.remove();

                if (key.isAcceptable()){
                    log.debug("触发了accept事件");
                    ServerSocketChannel sc = (ServerSocketChannel) key.channel();
                    SocketChannel socketChannel = sc.accept();
                    socketChannel.configureBlocking(false);
                    //将socketChannel注册到selector上
                    socketChannel.register(selector, SelectionKey.OP_READ);
                } else if (key.isReadable()){
                    log.debug("触发了read事件 {}", key);
                    SocketChannel sc = (SocketChannel) key.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(16);
                    int read = sc.read(buffer);
                    if (read != -1){
                        // 切换到 读模式
                        buffer.flip();
                        log.debug("接收到来自客户端的消息：{}", new String(buffer.array()));
                    } else {
                        key.cancel();
                    }
                }
            }
        }
    }
}
