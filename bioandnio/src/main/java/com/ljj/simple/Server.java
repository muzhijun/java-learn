package com.ljj.simple;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Server {

    public static void main(String[] args) throws IOException {

        ByteBuffer buffer = ByteBuffer.allocate(16);
        //创建一个serverSocketChannel
        ServerSocketChannel socketChannel = ServerSocketChannel.open();

        //监听端口
        socketChannel.bind(new InetSocketAddress(8080));

        List<SocketChannel> channelList =new ArrayList<>();
        while (true){
            log.debug("waiting connecting");
            SocketChannel channel = socketChannel.accept();
            log.debug("connected");
            channelList.add(channel);

            for (SocketChannel chan : channelList) {
                log.debug("before read ");
                chan.read(buffer);

                //切换到读模式
                buffer.flip();

                System.out.println("接收到客户端消息：" + new String(buffer.array()));

                buffer.clear();
                log.debug("after read");
            }
        }

    }
}
