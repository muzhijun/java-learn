package com.ljj.simple;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

@Slf4j
public class NServer1 {

    public static void main(String[] args) throws IOException {
        ServerSocketChannel serverSocketChannel =  ServerSocketChannel.open();
        //绑定端口
        serverSocketChannel.bind(new InetSocketAddress(8080));
        //设置非阻塞
        serverSocketChannel.configureBlocking(false);
        Selector selector  =  Selector.open();
        //将serverSocketChannel注册到Selector上，并设置监听的事件类型
        SelectionKey register = serverSocketChannel.register(selector, 0, null);
        register.interestOps(SelectionKey.OP_ACCEPT);
        log.debug("注册的selectionkey是{}", register);

        while (true){
            //select方法在没有连接 的时候会阻塞在这里
            log.debug("waiting connect");
            selector.select();
            Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
            while (keyIterator.hasNext()){
                log.debug("size:{}", selector.selectedKeys().size());
                SelectionKey key = keyIterator.next();
                //移除当前迭代的对象，防止重复读取
                keyIterator.remove();

                if (key.isAcceptable()){
                    log.debug("触发了accept事件");
                    ServerSocketChannel sc = (ServerSocketChannel) key.channel();
                    SocketChannel socketChannel = sc.accept();
                    socketChannel.configureBlocking(false);
                    //将socketChannel注册到selector上
                    ByteBuffer buffer = ByteBuffer.allocate(16);
                    SelectionKey selectionKey = socketChannel.register(selector, 0, buffer);
                    selectionKey.interestOps(SelectionKey.OP_READ);
                } else if (key.isReadable()){
                    log.debug("触发了read事件 {}", key);
                    SocketChannel sc = (SocketChannel) key.channel();
                    //从selectionkey中获取与selectionkey绑定的byteBuffer
                    ByteBuffer buffer = (ByteBuffer) key.attachment();
                    int read = sc.read(buffer);
                    if (read != -1){
                        split(buffer);
                        if (buffer.position() == buffer.limit()){//说明没有找到标志符，需要进行扩容
                            //扩容2倍
                            ByteBuffer newByteBuffer = ByteBuffer.allocate(buffer.capacity()*2);
                            // 切换到读模式
                            buffer.flip();
                            newByteBuffer.put(buffer);
                            //将新的ByteBuffer绑定到selectionKey上
                            key.attach(newByteBuffer);
                        }
                    } else {
                        key.cancel();
                    }
                }
            }
        }
    }


    private static void split(ByteBuffer source){
        source.flip();// 切换读模式
        for (int i = 0; i < source.limit(); i++) {
            if (source.get(i) == '\n'){// 查找到 \n 符号，则开始进行截取
                // 获取每个分割的长度
                int len = i - source.position() + 1;
                ByteBuffer target = ByteBuffer.allocate(len);
                //从source中循环读取放入到target中
                for (int j = 0; j < len; j++) {
                    target.put(source.get());
                }

                target.flip();//切换到读模式
                System.out.println(new String(target.array()));
            }
        }
        source.compact();// 切换写模式（保留未读部分）
    }
}
