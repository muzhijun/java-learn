package com.ljj.groupchat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.Set;

/***
 * @desc 群聊服务端
 * @Description
 * @date 2021/10/26 9:48 上午
 */
public class GroupChatServer {

    private Selector selector;// 定义selector
    private ServerSocketChannel listenChannel;//定义监听channel
    private static final int PORT = 6667;//定义端口

    // 初始化工作
    public GroupChatServer(){
        try {
            selector = Selector.open();
            listenChannel = ServerSocketChannel.open();
            listenChannel.socket().bind(new InetSocketAddress(PORT));
            listenChannel.configureBlocking(false);// 设置非阻塞
            //注册到selector
            listenChannel.register(selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //监听方法
    public void listen() {
        try {
            while (true){
                //没有事件会阻塞在这里
                int count = selector.select();
                if (count > 0){//说明有事件需要处理
                    Iterator<SelectionKey> selectionKeyIterator = selector.selectedKeys().iterator();
                    while (selectionKeyIterator.hasNext()){
                        SelectionKey next = selectionKeyIterator.next();
                        if (next.isAcceptable()){
                            acceptEvent();
                        }
                        if (next.isReadable()){
                            readEvent(next);
                        }
                    }
                    //删除当前事件，防止重复读取
                    selectionKeyIterator.remove();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     * @title 读事件
     * @Description
     * @author leadu
     * @date 2021/10/26 10:11 上午
     */
    public void readEvent(SelectionKey key) {
        //从selectionkey中查找到对应的socketchannel
        SocketChannel channel = null;
        try {
            channel = (SocketChannel) key.channel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            int read = channel.read(byteBuffer);
            if (read > 0){
                String msg = new String(byteBuffer.array());
                System.out.println("from 客户端" + msg);
                //向其他客户端发送消息-需要排除自己
                sendMsgToOtherClient(msg, channel);
            }
        } catch (IOException e) {
            try {
                System.out.println(channel.getRemoteAddress() + "设备离线了");
                key.cancel();
                channel.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /***
     * @title 写事件-上线
     * @Description
     * @author leadu
     * @date 2021/10/26 10:11 上午
     */
    public void acceptEvent(){
        try {
            SocketChannel socketChannel = listenChannel.accept();
            socketChannel.configureBlocking(false);
            socketChannel.register(selector, SelectionKey.OP_READ);
            System.out.println(socketChannel.getRemoteAddress() + "设备上线了");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @title 给其他客户端发送消息
     * @param msg
     * @param self
     * @Description
     * @author leadu
     * @date 2021/10/26 10:12 上午
     */
    public void sendMsgToOtherClient(String msg, SocketChannel self) throws IOException{
        System.out.println("服务器消息转发中");
        //遍历所有注册到seletor上的channel
        Set<SelectionKey> selectionKeySet = selector.keys();
        for (SelectionKey key : selectionKeySet){
            //根据key取出通道
            Channel channel = key.channel();
            if (channel instanceof SocketChannel && channel != self){
                SocketChannel socketChannel = (SocketChannel) channel;
                ByteBuffer byteBuffer = ByteBuffer.wrap(msg.getBytes());
                //将byteBuffer写入通道
                socketChannel.write(byteBuffer);
            }
        }
    }

    public static void main(String[] args) {
        GroupChatServer chatServer = new GroupChatServer();
        chatServer.listen();
    }
}
