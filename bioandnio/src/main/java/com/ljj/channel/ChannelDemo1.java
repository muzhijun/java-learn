package com.ljj.channel;

import lombok.extern.slf4j.Slf4j;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@Slf4j
public class ChannelDemo1 {

    public static void main(String[] args) {
        //需求：从data.txt文件读取内容，写入到ByteBuffer中

        try (RandomAccessFile randomAccessFile = new RandomAccessFile("data.txt", "rw")){
            FileChannel channel = randomAccessFile.getChannel();
            ByteBuffer byteBuffer = ByteBuffer.allocate(16);
            while (true){
                int len = channel.read(byteBuffer);
                //如果读取完成，则调用read方法返回的结果为-1
                if (len == -1){
                    break;
                }
                //切换为读模式
                byteBuffer.flip();
                while (byteBuffer.hasRemaining()){
                    /*
                    get方法会让读指针position往后走
                        如果需要重复读取，可以调用rewind方法将position重置为0
                        get(i)方法是获取到i的指针，不会移动position
                     */
                    log.debug(String.valueOf(byteBuffer.get()));
                }

                String str = new String(byteBuffer.array());
                System.out.println("写入到ByteBuffer中的内容是：" + str);
                byteBuffer.clear();
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
