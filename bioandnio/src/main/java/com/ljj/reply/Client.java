package com.ljj.reply;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

public class Client {
    public static void main(String[] args) throws IOException {
        //需求：建立与服务端的连接，从控制台监控录入的消息，发送给服务端，监听来自服务端的消息，打印到控制台

        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.connect(new InetSocketAddress("localhost", 8080));
        socketChannel.write(Charset.defaultCharset().encode("hello!"));
    }
}
