package com.ljj.reply;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
  
@Slf4j
public class Server {

    public static void main(String[] args) throws IOException {
        //需求：使用NIO创建服务端，接收服务端发送来的消息，然后转发回客户端

        ServerSocketChannel channel = ServerSocketChannel.open();
        //设置为非阻塞模式
        channel.configureBlocking(false);

        Selector selector = Selector.open();
        channel.bind(new InetSocketAddress(8080));
        channel.register(selector, SelectionKey.OP_ACCEPT);

        while (true){
            //监听事件，无事件则阻塞
            selector.select();

            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while(iterator.hasNext()){
                SelectionKey key = iterator.next();
                //手动移除当前事件，防止重复处理
                iterator.remove();

                //如果是连接事件
                if (key.isAcceptable()){
                    ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
                    SocketChannel socketChannel = ssc.accept();
                    log.debug("客户端连接成功...{}", socketChannel.getRemoteAddress());
                    socketChannel.configureBlocking(false);
                    socketChannel.register(selector, SelectionKey.OP_READ);
                }
                else if (key.isReadable()){
                    SocketChannel sc = (SocketChannel) key.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(6);
                    int read = sc.read(buffer);
                    if (read != -1){
                        // 切换到 读模式
                        buffer.flip();
                        log.debug("接收到来自客户端的消息：{}", new String(buffer.array()));
                    } else {
                        //如果不取消，每一次都会触发read事件
                        key.cancel();
                    }
                }
            }
        }
    }
}
