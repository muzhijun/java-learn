package com.example.mongoservice.test;

import com.example.mongoservice.MongoServiceApplication;
import com.example.mongoservice.model.Comment;
import com.example.mongoservice.service.CommentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/7 14:41
 */
//@RunWith(SpringRunner.class)
@SpringBootTest(classes = MongoServiceApplication.class)
public class CommentServiceTest {

    //注入Service
    @Autowired
    private CommentService commentService;

    /**
     * 保存一个评论
     */
    @Test
    public void testSaveComment() {
        Comment comment = new Comment();
        comment.setArticleid("100000");
        comment.setContent("测试添加的数据");
        comment.setCreatedatetime(LocalDateTime.now());
        comment.setUserid("1003");
        comment.setNickname("凯撒大帝");
        comment.setState("1");
        comment.setLikenum(0);
        comment.setReplynum(0);
        commentService.saveComment(comment);
    }

    /**
     * 查询所有数据
     */
    @Test
    public void testFindAll() {
        List<Comment> list = commentService.findCommentList();
        System.out.println(list);
    }

    /**
     * 测试根据id查询
     */
    @Test
    public void testFindCommentById() {
        Comment comment = commentService.findCommentById("624e88c80f430e08d8a36b79");
        System.out.println(comment);
    }

    /**
     * 测试分页查询评论信息
     */
    @Test
    public void testFindByParentid() {
        Page<Comment> page = commentService.findCommentListPageByParentid("3", 1, 2);
        int totalPages = page.getTotalPages();
        System.out.println("totalPage: " + totalPages);
        System.out.println("content" + page.getContent());
    }

    @Test
    public void testUpdateCommentThumbupToIncrementingOld() {
        commentService.updateCommentThumbupToIncrementingOld("624e88c80f430e08d8a36b79");
    }

    @Test
    public void updateCommentLikenum() {
        commentService.updateCommentLikenum("624e88c80f430e08d8a36b79");
    }
}
