package com.example.mongoservice;

import com.example.mongoservice.model.Comment;
import com.example.mongoservice.service.CommentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MongoServiceApplicationTests {

    @Autowired
    private CommentService commentService;

    @Test
    void contextLoads() {
    }

    /**
     * 查询所有数据
     */
    @Test
    public void testFindAll() {
        List<Comment> list = commentService.findCommentList();
        System.out.println(list);
    }

}
