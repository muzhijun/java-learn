package com.example.mongoservice.repository;

import com.example.mongoservice.model.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/7 14:32
 */
public interface CommentRepository extends MongoRepository<Comment, String> {

    Page<Comment> findByParentid(String parentid, Pageable pageable);
}
