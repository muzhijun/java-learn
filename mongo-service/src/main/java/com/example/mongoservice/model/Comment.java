package com.example.mongoservice.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/7 14:25
 */
@Getter
@Setter
@ToString
@Document(collection = "commentcopy")
public class Comment implements Serializable {

    @Id
    private String id;// 主键id
    private Date publishtime;// 发布日期
    private String content;// 吐槽内容
    //添加了一个单字段的索引
    @Indexed
    private String userid;//发布人ID
    private String nickname;//昵称
    private LocalDateTime createdatetime;//评论的日期时间
    private Integer likenum;//点赞数
    private Integer replynum;//回复数
    private String state;//状态
    private String parentid;//上级ID
    private String articleid;
}
