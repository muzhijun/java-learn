package com.example.mongoservice.service;

/**
 * @author lijunjun
 * @title 描述
 * @date 2022/4/7 14:34
 */

import com.example.mongoservice.model.Comment;
import com.example.mongoservice.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 保存comment信息
     */
    public void saveComment(Comment comment) {
        commentRepository.save(comment);
    }

    public List<Comment> findCommentList() {
        return commentRepository.findAll();
    }

    public Comment findCommentById(String id) {
        return commentRepository.findById(id).get();
    }

    public Page<Comment> findCommentListPageByParentid(String parentid, int page, int size) {
        Page<Comment> commentPage = commentRepository.findByParentid(parentid, PageRequest.of(page - 1, size));
        return commentPage;
    }

    /**
     * 更新点赞数
     *
     * @param id id
     */
    public void updateCommentThumbupToIncrementingOld(String id) {
        // 现查出id对应的整条数据，在进行更新
        Comment comment = commentRepository.findById(id).get();
        comment.setLikenum(comment.getLikenum() + 1);
        commentRepository.save(comment);
    }

    /**
     * 点赞数+1
     *
     * @param id id
     */
    public void updateCommentLikenum(String id) {
        Query query = new Query(Criteria.where("_id").is(id));
        Update update = new Update();
        // 局部更新，相当于$set
        //update.set(key, value);
        update.inc("likenum");
        System.out.println(mongoTemplate.updateFirst(query, update, Comment.class).getModifiedCount());
    }

}
